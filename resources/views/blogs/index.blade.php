@extends('layouts.admin')

@section('content')

<div class="blogs-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Blogs')}}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('blogs.create', __('words.Add Blog'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Image') }}</th>
                            <th>{{ __('words.Banner Image')}}</th>
                            <th>{{ __('words.Slug')}}</th>
                            <th>{{ __('words.Title') }}</th>
                            <th>{{ __('words.Meta Title') }}</th>
                            <th>{{ __('words.Meta Description') }}</th>
                            <th>{{ __('words.Keywords') }}</th>
                            <th>{{ __('words.Description') }}</th>
                            <th>{{ __('words.Author Name') }}</th>
                            <th>{{ __('words.Author Image') }}</th>
                            <th>{{ __('words.Author Content') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($blogs as $key => $blog)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ Html::image($blog->fImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ Html::image($blog->fBannerImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ $blog->slug }}</td>
                                <td>{{ $blog->title }}</td>
                                <td>{{ $blog->meta_title }}</td>
                                <td>{{ $blog->meta_description }}</td>
                                <td>{{ $blog->keywords }}</td>
                                <td>{{ $blog->description }}</td>
                                <td>{{ $blog->author_name }}</td>
                                <td>{{ Html::image($blog->fAuthorImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ $blog->author_content }}</td>
                                <td>{{ $blog->order }}</td>
                                <td>{{ $blog->language->name ?? null }}</td>
                                <td>{{ $blog->readableStatus }}</td>

                                <td class="d-flex">
                                    {{ Html::linkRoute('blogs.edit', __('button.Edit'), $blog->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('blogs.destroy', $blog->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $blogs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
