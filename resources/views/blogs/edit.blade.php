@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Blog') }}</h2>

{{ Form::open(['url' => route('blogs.update', $blog->id), 'class' => 'form-horizontal']) }}

@include('blogs._form')

{{ Form::close() }}

@endsection