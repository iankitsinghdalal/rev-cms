@if(isset($blog->fImage))
<div class="form-group">
    {{ Html::image($blog->fImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif 

<div class="form-group">
    {{ Form::label('image', __('Image'), []) }}
    {{ Form::select('image', [null => __('words.--Please Select--')] + $gallery, $blog->fImage->id ?? null, ['class' => 'form-control']) }}
    @error('image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

@if(isset($blog->fBannerImage))
<div class="form-group">
    {{ Html::image($blog->fBannerImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif 

<div class="form-group">
    {{ Form::label('banner_image', __('words.Banner Image'), []) }}
    {{ Form::select('banner_image', [null => __('words.--Please Select--')] + $gallery, $blog->fBannerImage->id ?? null, ['class' => 'form-control']) }}
    @error('banner_image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('slug', __('words.Slug'), []) }}
    {{ Form::text('slug', $blog->slug ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Slug')]) }}
    @error('slug')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('title', __('words.Title'), []) }}
    {{ Form::text('title', $blog->title ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Title')]) }}
    @error('title')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('meta_title', __('words.Meta Title'), []) }}
    {{ Form::text('meta_title', $blog->meta_title ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Meta Title')]) }}
    @error('meta_title')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('meta_description', __('words.Meta Description'), []) }}
    {{ Form::text('meta_description', $blog->meta_description ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Meta Description')]) }}
    @error('meta_description')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('keywords', __('words.Keywords'), []) }}
    {{ Form::text('keywords', $blog->keywords ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Keywords')]) }}
    @error('keywords')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('description', __('words.Description'), []) }}
    {{ Form::text('description', $blog->description ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Description')]) }}
    @error('description')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<!-- category -->

<div class="form-group">
    {{ Form::label('author_name', __('words.Author Name'), []) }}
    {{ Form::text('author_name', $blog->author_name ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Author Name')]) }}
    @error('author_name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

@if(isset($blog->fAuthorImage))
<div class="form-group">
    {{ Html::image($blog->fAuthorImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif 

<div class="form-group">
    {{ Form::label('author_image', __('words.Author Image'), []) }}
    {{ Form::select('author_image', [null => '--Please Select--'] + $gallery, $blog->fAuthorImage->id ?? null, ['class' => 'form-control']) }}
    @error('author_image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('author_content', __('words.Author Content'), []) }}
    {{ Form::text('author_content', $blog->author_content ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Author Content')]) }}
    @error('author_content')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $blog->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $blog->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button('Save', ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>