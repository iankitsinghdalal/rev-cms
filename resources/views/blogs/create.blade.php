@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('Add Blog') }}</h2>

{{ Form::open(['url' => route('blogs.store'), 'class' => 'form-horizontal']) }}

@include('blogs._form')

{{ Form::close() }}

@endsection