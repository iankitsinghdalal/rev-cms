<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                {{ Form::button("<span aria-hidden='true'>&times;</span>", ['class' => 'close', 'type' => 'button', 'data-dismiss' => 'modal', 'aria-label' => 'Close']) }}
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>