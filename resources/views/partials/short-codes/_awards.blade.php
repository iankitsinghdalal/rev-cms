@if(!count($awards))

<div class="info">{{ __('messages.NO_RECORD_FOUND') }}</div>

@else

<div class="row">
    <div class="col-md-12 text-center">
        <h2>Awards &amp; Recognition</h2>
        <p>We have earned awards worldwide for our commitment to excellence as a global leader and great employer</p>
    </div>
    <div class="col-md-12">
        <div class="slick-initialized slick-slider">
        <button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="">Previous</button>
        <div class="slick-list draggable" style="padding: 0px;">
            <div class="slick-track" style="opacity: 1; width: 4080px; transform: translate3d(-720px, 0px, 0px);">
                
                @foreach($awards as $key => $award)

                <div class="slick-item slick-slide slick-cloned" data-slick-index="{{ $key-5 }}" aria-hidden="true" style="width: 240px;" tabindex="-1">
                    <div class="img-wrap text-center">
                        <img class="img-fluid" src="{{ $award->gallery->path }}" alt="{{ $award->title }}" height="79" width="119">
                    </div>
                </div>

                @endforeach
            </div>
        </div>
        <button class="slick-next slick-arrow" aria-label="Next" type="button" style="">Next</button></div>
    </div>
</div>
<script type="text/javascript">
    (function () {
        $(".slick-track").slick({
            centerMode: true,
            centerPadding: "0px",
            slidesToShow: 4,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "0px",
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "0px",
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "0px",
                        slidesToShow: 1
                    }
                }
            ]
        });
    })();
</script>

@endif