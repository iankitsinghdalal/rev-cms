@if(!count($clients))

<div class="info">{{ __('messages.NO_RECORD_FOUND') }}</div>

@else

<div class="row">
    <div class="col-md-12 text-center">
        <h2>Clients</h2>
        <p>Tag Line</p>
    </div>
    <div class="col-md-12">
        <div class="slick-initialized slick-slider">
        <button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="">Previous</button>
        <div class="slick-list draggable" style="padding: 0px;">
            <div class="slick-track" style="opacity: 1; width: 4080px; transform: translate3d(-720px, 0px, 0px);">
                
                @foreach($clients as $key => $client)

                <div class="slick-item slick-slide slick-cloned" data-slick-index="{{ $key-5 }}" aria-hidden="true" style="width: 240px;" tabindex="-1">
                    <div class="img-wrap text-center">
                        <img class="img-fluid" src="{{ $client->gallery->path }}" alt="{{ $client->title }}" height="79" width="119">
                    </div>
                </div>

                @endforeach
            </div>
        </div>
        <button class="slick-next slick-arrow" aria-label="Next" type="button" style="">Next</button></div>
    </div>
</div>
<script type="text/javascript">
    (function () {
        $(".slick-track").slick({
            centerMode: true,
            centerPadding: "0px",
            slidesToShow: 4,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "0px",
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "0px",
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: "0px",
                        slidesToShow: 1
                    }
                }
            ]
        });
    })();
</script>

@endif