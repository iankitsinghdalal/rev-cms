@if(!count($industries))

<div class="info">{{ __('messages.NO_RECORD_FOUND') }}</div>

@else

<ul class="list-unstyled">
    @foreach($industries as $key => $industry)

    <li class="col-lg-4 mb-4 grid-list">
        <a href="https://www.revinfotech.com/ecommerce" class="indust-item position-relative">
            <div class="indstry-icon d-flex align-items-center" data-aos="zoom-in-up">
                <i class="{{ $industry->fa_icon }}"></i>
            </div>
            <h5 class="mt-0 sec-title">{{ $industry->title }}</h5>
        </a>
    </li>

    @endforeach
</ul>

@endif