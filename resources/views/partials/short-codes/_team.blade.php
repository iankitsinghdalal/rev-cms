@if(!count($team))

<div class="info">{{ __('messages.NO_RECORD_FOUND') }}</div>

@else
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <th>#</th>
            <th>{{ __('words.Photo') }}</th>
            <th>{{ __('words.Name') }}</th>
            <th>{{ __('words.Description') }}</th>
            <th>{{ __('words.Order') }}</th>
            <th>{{ __('words.Language') }}</th>
            <th>{{ __('words.Status') }}</th>
        </thead>
        <tbody>
            @foreach($team as $key => $member)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ Html::image($member->gallery->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                <td>{{ $member->name }}</td>
                <td>{{ $member->description }}</td>
                <td>{{ $member->order }}</td>
                <td>{{ $member->language->name ?? null }}</td>
                <td>{{ $member->readableStatus }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endif