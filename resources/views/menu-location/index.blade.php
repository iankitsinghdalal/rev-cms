@extends('layouts.admin')

@section('content')

<div class="menu-location-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Menu Locations') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('menu-location.create', __('words.Add Menu Location'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Name') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($menuLocations as $key => $menuLocation)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ __("words.$menuLocation->name") }}</td>
                                <td>{{ __("words.$menuLocation->readableStatus") }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('menu-location.edit', __('button.Edit'), $menuLocation->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('menu-location.destroy', $menuLocation->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $menuLocations->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
