@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Menu Location') }}</h2>

{{ Form::open(['url' => route('menu-location.store'), 'class' => 'form-horizontal']) }}

@include('menu-location._form')

{{ Form::close() }}

@endsection