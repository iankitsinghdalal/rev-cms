<div class="form-group">
    {{ Form::label('name', 'Name: ', []) }}
    {{ Form::text('name', isset($user->name) ? $user->name : null, ['class' => 'form-control', 'placeholder' => 'Enter Name']) }}
    @error('name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('email', 'E-Mail Address: ', []) }}
    {{ Form::email('email', isset($user->email) ? $user->email : null, ['class' => 'form-control', 'placeholder' => 'Enter Email']) }}
    @error('email')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

@if(isset($user->photo))
<div class="form-group">
    {{ Html::image($user->photo, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif

<div class="form-group">
    {{ Form::label('photo', 'Choose Image ', ['class' => 'btn btn-primary']) }}
    {{ Form::file('photo') }}
    @error('photo')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button('Save', ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>


