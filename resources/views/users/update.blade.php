@extends('layout.master')

@section('content')

<h2 class="float-left">Update User</h2>

{{ Form::open(['url' => route('update', $user->id), 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}

@include('users._form')

{{ Form::close() }}

@endsection