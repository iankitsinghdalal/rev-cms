@extends('layout.master')

@section('content')

<h2 class="float-left">Create User</h2>

{{ Form::open(['url' => route('store'), 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}

@include('users._form')

{{ Form::close() }}

@endsection