@extends('layout.master')

@section('content')

<table class="table table-responsive">
    <thead>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
        <th>Photo</th>
        <th colspan="2">
    </thead>
    <tbody>
        @foreach($users as $key => $user)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ Html::image($user->photo, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
            <td>{{ Html::linkRoute('edit', 'Edit', ['id' => $user->id], ['class' => 'btn btn-info']) }}</td>
            <td>
                {{ Form::open(['url' => route('destroy', $user->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                {{ Form::button('Delete', ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
