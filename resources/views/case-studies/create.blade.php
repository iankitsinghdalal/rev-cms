@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('Add Case Study') }}</h2>

{{ Form::open(['url' => route('case-studies.store'), 'class' => 'form-horizontal']) }}

@include('case-studies._form')

{{ Form::close() }}

@endsection