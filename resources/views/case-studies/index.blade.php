@extends('layouts.admin')

@section('content')

<div class="case-study-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Case Studies') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('case-studies.create', __('words.Add Case Study'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Image') }}</th>
                            <th>{{ __('words.Banner Image') }}</th>
                            <th>{{ __('words.Slug') }}</th>
                            <th>{{ __('words.Title')}}</th>
                            <th>{{ __('words.Meta Title') }}</th>
                            <th>{{ __('words.Meta Description') }}</th>
                            <th>{{ __('words.Keywords') }}</th>
                            <th>{{ __('words.Description') }}</th>
                            <th>{{ __('words.Author Name') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($caseStudies as $key => $caseStudy)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ Html::image($caseStudy->fImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ Html::image($caseStudy->fBannerImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ $caseStudy->slug }}</td>
                                <td>{{ $caseStudy->title }}</td>
                                <td>{{ $caseStudy->meta_title }}</td>
                                <td>{{ $caseStudy->meta_description }}</td>
                                <td>{{ $caseStudy->keywords }}</td>
                                <td>{{ $caseStudy->description }}</td>
                                <td>{{ $caseStudy->author_name }}</td>
                                <td>{{ $caseStudy->order }}</td>
                                <td>{{ $caseStudy->language->name ?? null }}</td>
                                <td>{{ $caseStudy->readableStatus }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('case-studies.edit', __('button.Edit'), $caseStudy->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('case-studies.destroy', $caseStudy->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $caseStudies->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
