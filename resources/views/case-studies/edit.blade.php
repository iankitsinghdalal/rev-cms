@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Case Study') }}</h2>

{{ Form::open(['url' => route('case-studies.update', $caseStudy->id), 'class' => 'form-horizontal']) }}

@include('case-studies._form')

{{ Form::close() }}

@endsection