@if(isset($caseStudy->fImage))
<div class="form-group">
    {{ Html::image($caseStudy->fImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif 

<div class="form-group">
    {{ Form::label('image', __('words.Image'), []) }}
    {{ Form::select('image', [null => __('words.--Please Select--')] + $gallery, $caseStudy->fImage->id ?? null, ['class' => 'form-control']) }}
    @error('image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

@if(isset($caseStudy->fBannerImage))
<div class="form-group">
    {{ Html::image($caseStudy->fBannerImage->path, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif 

<div class="form-group">
    {{ Form::label('banner_image', __('words.Banner Image'), []) }}
    {{ Form::select('banner_image', [null => __('words.--Please Select--')] + $gallery, $caseStudy->fBannerImage->id ?? null, ['class' => 'form-control']) }}
    @error('banner_image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('slug', __('words.Slug'), []) }}
    {{ Form::text('slug', $caseStudy->slug ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Slug')]) }}
    @error('slug')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('title', __('words.Title'), []) }}
    {{ Form::text('title', $caseStudy->title ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Title')]) }}
    @error('title')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('meta_title', __('words.Meta Title'), []) }}
    {{ Form::text('meta_title', $caseStudy->meta_title ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Meta Title')]) }}
    @error('meta_title')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('meta_description', __('words.Meta Description'), []) }}
    {{ Form::text('meta_description', $caseStudy->meta_description ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Meta Description')]) }}
    @error('meta_description')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('keywords', __('words.Keywords'), []) }}
    {{ Form::text('keywords', $caseStudy->keywords ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Keywords')]) }}
    @error('keywords')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('description', __('words.Description'), []) }}
    {{ Form::text('description', $caseStudy->description ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Description')]) }}
    @error('description')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<!-- category -->

<div class="form-group">
    {{ Form::label('author_name', __('words.Author Name'), []) }}
    {{ Form::text('author_name', $caseStudy->author_name ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Author Name')]) }}
    @error('author_name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $caseStudy->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $caseStudy->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>