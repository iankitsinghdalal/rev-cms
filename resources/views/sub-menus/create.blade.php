@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Sub Menu') }}</h2>

{{ Form::open(['url' => route('sub-menus.store'), 'class' => 'form-horizontal']) }}

@include('sub-menus._form')

{{ Form::close() }}

@endsection