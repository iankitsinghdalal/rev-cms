@extends('layouts.admin')

@section('content')

<div class="sub-menus-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Sub Menus') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('sub-menus.create', __('words.Add Sub Menu'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Name') }}</th>
                            <th>{{ __('words.Menu') }}</th>
                            <th>{{ __('words.Location') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($subMenus as $key => $subMenu)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $subMenu->name }}</td>
                                <td>{{ $subMenu->menu->name ?? null }}</td>
                                <td>{{ $subMenu->menu->location->name ?? null }}</td>
                                <td>{{ $subMenu->menu->language->name ?? null }}</td>
                                <td>{{ $subMenu->readableStatus }}</td>

                                <td class="d-flex">
                                    {{ Html::linkRoute('sub-menus.edit', __('button.Edit'), $subMenu->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('sub-menus.destroy', $subMenu->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $subMenus->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
