<div class="form-group">
    {{ Form::label('name', __('words.Name'), []) }}
    {{ Form::text('name', $subMenu->name ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Title')]) }}
    @error('name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('menu_id', __('words.Menu'), []) }}
    {{ Form::select('menu_id', [null => __('words.--Please Select--')] + $menus, $subMenu->menu->id ?? null, ['class' => 'form-control']) }}
    @error('menu_id')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $subMenu->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $subMenu->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>