<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
    <div class="container-fluid">          
    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#sociallinkAdd">
        Add Links
    </button>
    <table class="table table-bordered category_table">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Social Link</th>
                <th>URL</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @foreach($socialdata as $socialvalue)
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $socialvalue->social }}</td>
                    <td>{{ $socialvalue->url }}</td>
                    <td>
                        <button class="btn btn-success" data-url="{{$socialvalue->url}}" data-social="{{$socialvalue->social}}" data-toggle="modal" onClick="editSocial(this,{{$socialvalue->id}})">Edit</button>
                        <button class="btn btn-danger" onClick="deleteSocial(this,{{$socialvalue->id}})">Delete</button>
                    </td>
                </tr>
            <?php $i++; ?>	
            @endforeach
        </tbody>
    </table>
</div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="sociallinkAdd">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                <h4 class="modal-title">Insert Social Links</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form method="post" action="{{url('admin/social-request')}}" enctype="multipart/form-data">
                        @csrf
                          <div class="form-group">
                            <label for="keyword">Select Social</label>
                              <select class="form-control" name="social" required>
                                <option value="">Please choose an option</option>
                                <option value="facebook">Facebook</option>
                                <option value="twitter">Twitter</option>
                                <option value="youtube">Youtube</option>
                                <option value="instagram">Instagram</option>
                                <option value="linkedin">LinkedIn</option>
                              </select>
                          </div>

                          <div class="form-group">
                            <label for="url">URL</label>
                            <input type="text" class="form-control" id="url" name="url" placeholder="Add Social Url">
                          </div>

                        <!-- /.box-body -->
                        <button type="submit" class="btn btn-primary">Submit</button>	
                    </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div> 
            </div>
        </div>
    </div>

<!-- Delete Social Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this record?
      </div>
      <div class="modal-footer">
        <form method="POST" action="{{url('admin/socialdelete')}}">
          @csrf
          <input type="hidden" name="delid" id="delbtn"/>
          <button type="submit" class="btn btn-danger">Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Edit Modal of socila Links-->
<div class="modal fade" id="sociallinkEdit">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Social Links</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
        <form method="post" action="{{url('admin/social-update')}}" enctype="multipart/form-data">
      <div class="modal-body">
          @csrf
          <input type="hidden" name="socialedit" id="socialeditbtn" />

            <div class="form-group">
              <label for="keyword">Select Social Name</label>
                <select class="form-control" id="getsocial" name="social" required>
                  <option value="">--Please choose an option--</option>
                  <option value="facebook" id="facebook">Facebook url</option>
                  <option value="twitter" id="twitter">Twitter url</option>
                  <option value="youtube" id="youtube">Youtube url</option>
                  <option value="instagram" id="instagram">Instagram url</option>
                  <option value="linkedin" id="linkedin">LinkedIn url</option>
                </select>
            </div>

            <div class="form-group">
              <label for="url">URL</label>
              <input type="text" class="form-control" id="geturl" name="url">
            </div>
          <!-- /.box-body -->
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Update</button>	
      </div> 
    </div>
        </form>
  </div>
</div>