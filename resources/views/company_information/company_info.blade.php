<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    
    <div class="container-fluid">          
    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#myModal">
        Add Details
    </button>
        <table class="table table-bordered category_table">
            <thead>
            <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                <?php $i=1; ?>
                @foreach($view as $infovalue)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $infovalue->name }}</td>
                        <td>{{ $infovalue->address }}</td>
                        <td>{{ $infovalue->phone }}</td>
                        <td>
                            <button class="btn btn-success" data-name="{{$infovalue->name}}" data-phone="{{$infovalue->phone}}" data-address="{{$infovalue->address}}" data-toggle="modal" onClick="editCompanyInfo(this,{{$infovalue->id}})">Edit</button>
                            <button class="btn btn-danger" onClick="deleteInfo(this,{{$infovalue->id}})">Delete</button>
                        </td>
                    </tr>
                <?php $i++; ?>	
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Delete Inforamtion Modal -->
<div class="modal fade" id="deleteinformaton" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this record?
      </div>
      <div class="modal-footer">
        <form method="POST" action="{{url('admin/infodelete')}}">
          @csrf
          <input type="hidden" name="delinfoid" id="delinfobtn" />
          <button type="submit" class="btn btn-danger">Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="myModal">
        <div class="modal-dialog modal-sm">
                <form action="{{url('admin/info-request')}}" method="POST">
            <div class="modal-content">
            
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Insert Location Details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="name">Location Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter Location" name="name">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea class="form-control" id="address" name="address" rows="3" placeholder="Enter Address"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" id="phone" placeholder="Enter Phone Number" name="phone">
                    </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            
            </div>
                </form>
        </div>
    </div>
            
<!-- Edit Modal -->
    <div class="modal fade" id="CompanyInfoEdit">
        <div class="modal-dialog modal-sm">
            <form action="{{url('admin/information-update')}}" method="POST">
            <div class="modal-content">
            
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit Location Details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                    @csrf
                    <input type="hidden" name="compnayinfobtn" id="compnayinfobtn"/>
                    <div class="form-group">
                        <label for="name">Location Name</label>
                        <input type="text" class="form-control" id="updatename" placeholder="Enter Location" name="name">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea class="form-control" id="updateaddress" name="address" rows="3" placeholder="Enter Address"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" id="updatephone" placeholder="Enter Phone Number" name="phone">
                    </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            
            </div>
                </form>
        </div>
    </div>