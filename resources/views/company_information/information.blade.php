@include('adminHome')
 <div class="main-header mt-3">
    <div class="container-fluid">
        <div class="tab-wrap">
          <ul class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><b>General</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><b>Company Information</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><b>Social Links</b></a>
            </li>
          </ul>
        <div class="tab-content" id="pills-tabContent">
          @include('company_information.general')
          @include('company_information.company_info')
          @include('company_information.sociallinks')
        </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	function deleteSocial(delthis, id){
		$('#exampleModal').modal('show');
		$('#delbtn').val(id);
	}
  function editSocial(delthis, id)
  {
    var updateid = $(delthis).data('id');
		var url = $(delthis).data('url');
    var social = $(delthis).data('social');
    $('#sociallinkEdit').modal('show');
		$('#socialeditbtn').val(id);
    $('#geturl').val(url);
    $('#'+social).prop('selected',true);
	}

  function deleteInfo(delthis, id){
		$('#deleteinformaton').modal('show');
		$('#delinfobtn').val(id);
	}

  function editCompanyInfo(delthis, id){
    var name = $(delthis).data('name');
    var phone = $(delthis).data('phone');
    var address = $(delthis).data('address');
    $('#CompanyInfoEdit').modal('show');
    $('#compnayinfobtn').val(id);
    $('#updatename').val(name);
    $('#updatephone').val(phone);
    $('#updateaddress').val(address);
	}
</script>
