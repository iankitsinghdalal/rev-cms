@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Menu Location') }}</h2>

{{ Form::open(['url' => route('menus.update', $menu->id), 'class' => 'form-horizontal']) }}

@include('menus._form')

{{ Form::close() }}

@endsection