@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Menu') }}</h2>

{{ Form::open(['url' => route('menus.store'), 'class' => 'form-horizontal']) }}

@include('menus._form')

{{ Form::close() }}

@endsection