@extends('layouts.admin')

@section('content')

<div class="menus-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Menus') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('menus.create', __('words.Add Menu'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Name') }}</th>
                            <th>{{ __('words.Location') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($menus as $key => $menu)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $menu->name }}</td>
                                <td>{{ $menu->location->name ?? null }}</td>
                                <td>{{ $menu->order }}</td>
                                <td>{{ $menu->language->name ?? null }}</td>
                                <td>{{ $menu->readableStatus }}</td>

                                <td class="d-flex">
                                    {{ Html::linkRoute('menus.edit', __('button.Edit'), $menu->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('menus.destroy', $menu->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $menus->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
