<div class="form-group">
    {{ Form::label('name', __('words.Name'), []) }}
    {{ Form::text('name', $menu->name ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Name')]) }}
    @error('name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('location', __('words.Location'), []) }}
    {{ Form::select('location_id', [null => __('words.--Please Select--')] + $menuLocations, $menu->location->id ?? null, ['class' => 'form-control']) }}
    @error('location_id')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $menu->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $menu->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>