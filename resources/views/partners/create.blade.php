@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Partner') }}</h2>

{{ Form::open(['url' => route('partners.store'), 'class' => 'form-horizontal']) }}

@include('partners._form')

{{ Form::close() }}

@endsection