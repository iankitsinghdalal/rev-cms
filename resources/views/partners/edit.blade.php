@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Partner') }}</h2>

{{ Form::open(['url' => route('partners.update', $partner->id), 'class' => 'form-horizontal']) }}

@include('partners._form')

{{ Form::close() }}

@endsection