@extends('layouts.admin')

@section('content')

<div class="partners-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Partners') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('partners.create', __('words.Add Partner'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Photo') }}</th>
                            <th>{{ __('words.Title') }}</th>
                            <th>{{ __('words.Description') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($partners as $key => $partner)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ Html::image($partner->gallery->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ $partner->title }}</td>
                                <td>{{ $partner->description }}</td>
                                <td>{{ $partner->order }}</td>
                                <td>{{ $partner->language->name ?? null }}</td>
                                <td>{{ $partner->readableStatus }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('partners.edit', __('button.Edit'), $partner->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('partners.destroy', $partner->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $partners->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
