@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Portfolio') }}</h2>

{{ Form::open(['url' => route('portfolios.store'), 'class' => 'form-horizontal']) }}

@include('portfolios._form')

{{ Form::close() }}

@endsection