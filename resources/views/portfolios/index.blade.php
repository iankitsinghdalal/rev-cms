@extends('layouts.admin')

@section('content')

<div class="portfolios-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Portfolios') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('portfolios.create', __('words.Add Portfolio'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Logo') }}</th>
                            <th>{{ __('words.Feature Image') }}</th>
                            <th>{{ __('words.Normal Images') }}</th>
                            <th>{{ __('words.Description') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($portfolios as $key => $portfolio)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ Html::image($portfolio->flogo->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ Html::image($portfolio->ffeatureimage->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ json_encode($portfolio->normal_images) }}</td>
                                <td>{{ $portfolio->description }}</td>
                                <td>{{ $portfolio->order }}</td>
                                <td>{{ $portfolio->language->name ?? null }}</td>
                                <td>{{ $portfolio->readableStatus }}</td>

                                <td class="d-flex">
                                    {{ Html::linkRoute('portfolios.edit', __('button.Edit'), $portfolio->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('portfolios.destroy', $portfolio->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $portfolios->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
