<div class="form-group">
    {{ Form::label('logo', __('words.Logo'), []) }}
    {{ Form::select('logo', [null => __('words.--Please Select--')] + $gallery, $portfolio->logo ?? null, ['class' => 'form-control']) }}
    @error('logo')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('feature_image', __('words.Feature Image'), []) }}
    {{ Form::select('feature_image', [null => __('words.--Please Select--')] + $gallery, $portfolio->feature_image ?? null, ['class' => 'form-control']) }}
    @error('feature_image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('normal_images', __('words.Normal Images'), []) }}
    <select multiple="multiple" required="required" class="form-control" name="normal_images[]">
        <option value="">{{ __('words.--Please Select--') }}</option>
        @foreach ($gallery as $key => $value)
            <option value="{{ $key }}" {{ isset($portfolio) && in_array($key, $portfolio->normal_images) ? 'selected' : null }}>{{ $value }}</option>
        @endforeach
    </select>
    @error('normal_images')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
    @error('normal_images.*')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('description', __('words.Description'), []) }}
    {{ Form::text('description', $portfolio->description ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Description')]) }}
    @error('description')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $portfolio->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $portfolio->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>