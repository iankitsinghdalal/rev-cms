@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Portfolio') }}</h2>

{{ Form::open(['url' => route('portfolios.update', $portfolio->id), 'class' => 'form-horizontal']) }}

@include('portfolios._form')

{{ Form::close() }}

@endsection