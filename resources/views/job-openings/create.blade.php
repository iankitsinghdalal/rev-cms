@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Job Opening') }}</h2>

{{ Form::open(['url' => route('job-openings.store'), 'class' => 'form-horizontal']) }}

@include('job-openings._form')

{{ Form::close() }}

@endsection