@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Job Opening')}}</h2>

{{ Form::open(['url' => route('job-openings.update', $jobOpening->id), 'class' => 'form-horizontal']) }}

@include('job-openings._form')

{{ Form::close() }}

@endsection