@extends('layouts.admin')

@section('content')

<div class="job-openings-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Job Openings') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('job-openings.create', __('words.Add Job Opening'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Position') }}</th>
                            <th>{{ __('words.Department') }}</th>
                            <th>{{ __('words.Experience Required') }}</th>
                            <th>{{ __('words.Location')}}</th>
                            <th>{{ __('words.Sharp Skills') }}</th>
                            <th>{{ __('words.Skills') }}</th>
                            <th>{{ __('words.Job Description') }}</th>
                            <th>{{ __('words.Remarks') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($jobOpenings as $key => $jobOpening)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $jobOpening->position }}</td>
                                <td>{{ $jobOpening->department }}</td>
                                <td>{{ $jobOpening->experience_required }}</td>
                                <td>{{ $jobOpening->location }}</td>
                                <td>{{ $jobOpening->sharp_skills }}</td>
                                <td>{{ $jobOpening->skills }}</td>
                                <td>{{ $jobOpening->job_description }}</td>
                                <td>{{ $jobOpening->remarks }}</td>
                                <td>{{ $jobOpening->order }}</td>
                                <td>{{ $jobOpening->language->name ?? null }}</td>
                                <td>{{ $jobOpening->readableStatus }}</td>

                                <td class="d-flex">
                                    {{ Html::linkRoute('job-openings.edit', __('button.Edit'), $jobOpening->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('job-openings.destroy', $jobOpening->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $jobOpenings->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
