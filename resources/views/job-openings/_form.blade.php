<div class="form-group">
    {{ Form::label('position', __('words.Position'), []) }}
    {{ Form::text('position', $jobOpening->position ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Position')]) }}
    @error('position')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('department', __('words.Department'), []) }}
    {{ Form::text('department', $jobOpening->department ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Department')]) }}
    @error('department')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('experience_required', __('words.Experience Required'), []) }}
    {{ Form::text('experience_required', $jobOpening->experience_required ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Title')]) }}
    @error('experience_required')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('location', __('Location'), []) }}
    {{ Form::text('location', $jobOpening->location ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Location')]) }}
    @error('location')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('sharp_skills', __('words.Sharp Skills'), []) }}
    {{ Form::text('sharp_skills', $jobOpening->sharp_skills ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Sharp Skills')]) }}
    @error('sharp_skills')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('skills', __('words.Skills'), []) }}
    {{ Form::text('skills', $jobOpening->skills ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Title')]) }}
    @error('skills')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('job_description', __('words.Job Description'), []) }}
    {{ Form::text('job_description', $jobOpening->job_description ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Job Description')]) }}
    @error('job_description')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('remarks', __('words.Remarks'), []) }}
    {{ Form::text('remarks', $jobOpening->remarks ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Remarks')]) }}
    @error('remarks')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $jobOpening->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $jobOpening->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>