@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Team Member') }}</h2>

{{ Form::open(['url' => route('team.update', $member->id), 'class' => 'form-horizontal']) }}

@include('team._form')

{{ Form::close() }}

@endsection