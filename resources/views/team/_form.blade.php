@if(isset($member->gallery))
<div class="form-group">
    {{ Html::image($member->gallery->path, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif

<div class="form-group">
    {{ Form::label('image', __('words.Image'), []) }}
    {{ Form::select('image', [null => __('words.--Please Select--')] + $gallery, $member->gallery->id ?? null, ['class' => 'form-control']) }}
    @error('image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('name', __('words.Name'), []) }}
    {{ Form::text('name', $member->name ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Name')]) }}
    @error('name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('description', 'Description: ', []) }}
    {{ Form::text('description', $member->description ?? null, ['class' => 'form-control', 'placeholder' => 'Enter Description']) }}
    @error('description')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $member->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $member->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>