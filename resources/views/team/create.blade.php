@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Team Member') }}</h2>

{{ Form::open(['url' => route('team.store'), 'class' => 'form-horizontal']) }}

@include('team._form')

{{ Form::close() }}

@endsection