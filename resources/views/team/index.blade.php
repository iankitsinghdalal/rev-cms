@extends('layouts.admin')

@section('content')

<div class="team-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">Team</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('team.create', 'Add Team Member', null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Photo') }}</th>
                            <th>{{ __('words.Name') }}</th>
                            <th>{{ __('words.Description') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($team as $key => $member)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ Html::image($member->gallery->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ $member->name }}</td>
                                <td>{{ $member->description }}</td>
                                <td>{{ $member->order }}</td>
                                <td>{{ $member->language->name ?? null }}</td>
                                <td>{{ $member->readableStatus }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('team.edit', __('button.Edit'), $member->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('team.destroy', $member->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $team->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
