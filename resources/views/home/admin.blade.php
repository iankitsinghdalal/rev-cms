@extends('layouts.admin')

@section('content')

<h1>Admin [Role:: {{ auth()->user()->role }}] Home</h1>

@include('home._')

@endsection