<div class="form-group">
    {{ Form::label('name', __('words.Name'), []) }}
    {{ Form::text('name', $language->name ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Name')]) }}
    @error('name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('short_name', __('words.Short Name'), []) }}
    {{ Form::text('short_name', $language->short_name ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Short Name')]) }}
    @error('short_name')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $language->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>