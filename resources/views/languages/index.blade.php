@extends('layouts.admin')

@section('content')

<div class="languages-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Languages') }}</h2>
                <div class="filter-block float-right">
                    {{ Form::button(__('button.Add Language'), ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modal', 'data-url' => route('languages.feed-modal')]) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Name') }}</th>
                            <th>{{ __('words.Short Name') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($languages as $key => $language)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ __("words.$language->name") }}</td>
                                <td>{{ __("words.$language->short_name") }}</td>
                                <td>{{ __("words.$language->readableStatus") }}</td>

                                <td class="d-flex">
                                    {{ Html::linkRoute('languages.edit', __('button.Edit'), $language->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('languages.destroy', $language->id), 'onsubmit' => 'return confirm(__("Are you sure?"))', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                        {{ $languages->links() }}
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@include('partials.modal')

@endsection

@section('js')
<script type="text/javascript">
    (function () {
        var Modal = {
            init: function () {
                this.getForm();
            },
            getForm: function() {
                (function () {
                    $('#modal').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget) // Button that triggered the modal
                        var recipient = button.data('url') // Extract info from data-* attributes
                        
                        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                        var modal = $(this)
                        modal.find('.modal-title').text( "{{ __('words.Add Language') }}" );
                        modal.find('.modal-body').load(recipient);
                    });
                })();
            },
            submitForm: function() {
                (function () {
                    $("modal-form").submit(function (event) {
                        event.preventDefault();
                    });
                })();
            }
        };

        Modal.init();
    })();
</script>
@append
