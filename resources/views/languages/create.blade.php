{{ Form::open(['url' => route('languages.store'), 'class' => 'form-horizontal', 'name' => 'modal-form']) }}

@include('languages._form')

{{ Form::close() }}