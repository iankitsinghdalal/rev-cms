@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Language') }}</h2>

{{ Form::open(['url' => route('languages.update', $language->id), 'class' => 'form-horizontal']) }}

@include('languages._form')

{{ Form::close() }}

@endsection