@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Award') }}</h2>

{{ Form::open(['url' => route('awards.update', $award->id), 'class' => 'form-horizontal']) }}

@include('awards._form')

{{ Form::close() }}

@endsection