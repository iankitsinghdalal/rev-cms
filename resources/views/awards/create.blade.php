@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Award') }}</h2>

{{ Form::open(['url' => route('awards.store'), 'class' => 'form-horizontal']) }}

@include('awards._form')

{{ Form::close() }}

@endsection