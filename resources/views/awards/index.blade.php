@extends('layouts.admin')

@section('title')
{{ __('words.Awards') }}
@endsection

@section('content')

<div class="awards-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Awards') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('awards.create', __('Add Award'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Photo') }}</th>
                            <th>{{ __('words.Title') }}</th>
                            <th>{{ __('words.Description') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($awards as $key => $award)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ Html::image($award->gallery->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ $award->title }}</td>
                                <td>{{ $award->description }}</td>
                                <td>{{ $award->order }}</td>
                                <td>{{ $award->language->name ?? null }}</td>
                                <td>{{ $award->readableStatus }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('awards.edit', __('button.Edit'), $award->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('awards.destroy', $award->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $awards->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
