@if(isset($client->gallery))
<div class="form-group">
    {{ Html::image($client->gallery->path, 'avtar', ['width' => '200', 'height' => '200']) }}
</div>
@endif

<div class="form-group">
    {{ Form::label('image', __('words.Image'), []) }}
    {{ Form::select('image', [null => __('words.--Please Select--')] + $gallery, $client->gallery->id ?? null, ['class' => 'form-control']) }}
    @error('image')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('title', __('words.Title'), []) }}
    {{ Form::text('title', $client->title ?? null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) }}
    @error('title')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $client->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $client->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>