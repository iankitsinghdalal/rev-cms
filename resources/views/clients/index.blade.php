@extends('layouts.admin')

@section('content')

<div class="clients-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Clients') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('clients.create', __('words.Add Client'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Photo') }}</th>
                            <th>{{ __('words.Title') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($clients as $key => $client)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ Html::image($client->gallery->path, 'avtar', ['width' => '200', 'height' => '200']) }}</td>
                                <td>{{ $client->title }}</td>
                                <td>{{ $client->order }}</td>
                                <td>{{ $client->language->name ?? null }}</td>
                                <td>{{ $client->readableStatus }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('clients.edit', __('button.Edit'), $client->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('clients.destroy', $client->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $clients->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
