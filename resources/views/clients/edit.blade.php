@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Client') }}</h2>

{{ Form::open(['url' => route('clients.update', $client->id), 'class' => 'form-horizontal']) }}

@include('clients._form')

{{ Form::close() }}

@endsection