@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Add Client') }}</h2>

{{ Form::open(['url' => route('clients.store'), 'class' => 'form-horizontal']) }}

@include('clients._form')

{{ Form::close() }}

@endsection