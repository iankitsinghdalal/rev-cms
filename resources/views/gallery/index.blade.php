@extends('layouts.admin')

@section('content')

<div class="gallery-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Gallery') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('gallery.create', __('words.Add New Image'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix" id="myMosaic">
                @foreach($gallery as $image)

                <img src="{{ $image->path }}" alt="{{ $image->title }}" width="400" height="350" />

                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    (function () {
        $('#myMosaic').Mosaic({
            maxRowHeight: 250,
            refitOnResize: true,
            refitOnResizeDelay: false,
            defaultAspectRatio: 0.5,
            maxRowHeightPolicy: 'crop',
            highResImagesWidthThreshold: 850,
            responsiveWidthThreshold: 500
        });
    })();
</script>
@append
