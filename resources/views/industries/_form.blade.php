<div class="form-group">
    {{ Form::label('title', __('words.Title'), []) }}
    {{ Form::text('title', $industry->title ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Title')]) }}
    @error('title')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('fa_icon', __('words.Fa Icon'), []) }}
    {{ Form::text('fa_icon', $industry->fa_icon ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Fa Icon Class')]) }}
    @error('fa_icon')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('status', __('words.Status'), []) }}
    {{ Form::select('status', [null => __('words.--Please Select--')] + [__('words.Inactive'), __('words.Active')], $industry->status ?? null, ['class' => 'form-control']) }}
    @error('status')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::label('order', __('words.Order'), []) }}
    {{ Form::number('order', $industry->order ?? null, ['class' => 'form-control', 'placeholder' => __('words.Enter Order'), 'min' => 0, 'max' => 10]) }}
    @error('order')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button(__('button.Save'), ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>