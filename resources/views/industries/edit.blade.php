@extends('layouts.admin')

@section('content')

<h2 class="float-left">{{ __('words.Update Industry') }}</h2>

{{ Form::open(['url' => route('industries.update', $industry->id), 'class' => 'form-horizontal']) }}

@include('industries._form')

{{ Form::close() }}

@endsection