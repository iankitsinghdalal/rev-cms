@extends('layouts.admin')

@section('content')

<div class="industries-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">{{ __('words.Industries') }}</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('industries.create', __('words.Add Industry'), null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>{{ __('words.Title') }}</th>
                            <th>{{ __('words.Fa Icon') }}</th>
                            <th>{{ __('words.Order') }}</th>
                            <th>{{ __('words.Language') }}</th>
                            <th>{{ __('words.Status') }}</th>
                            <th>{{ __('words.Actions') }}</th>
                        </thead>
                        <tbody>
                            @foreach($industries as $key => $industry)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $industry->title }}</td>
                                <td><i class="{{ $industry->fa_icon }}"></td>
                                <td>{{ $industry->order }}</td>
                                <td>{{ $industry->language->name ?? null }}</td>
                                <td>{{ $industry->readableStatus }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('industries.edit', __('button.Edit'), $industry->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('industries.destroy', $industry->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button(__('button.Delete'), ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $industries->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
