@extends('layouts.admin')

@section('content')

<div class="pages-index">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="float-left">Pages</h2>
                <div class="filter-block float-right">
                    {{ Html::linkRoute('pages.create', 'Add Page', null, ['class' => 'btn btn-success']) }}
                </div>
            </header>
            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Slug</th>
                            <th>Actions</th>
                        </thead>
                        <tbody>
                            @foreach($pages as $key => $page)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $page->slug }}</td>
                                
                                <td class="d-flex">
                                    {{ Html::linkRoute('pages.edit', 'Edit', $page->id, ['class' => 'btn btn-info mr-2']) }}
                                    {{ Form::open(['url' => route('pages.destroy', $page->id), 'onsubmit' => 'return confirm("Are you sure?")', 'method' => 'DELETE']) }}
                                    {{ Form::button('Delete', ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $pages->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
