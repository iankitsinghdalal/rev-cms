@extends('layouts.admin')

@section('content')

<h2 class="float-left">Add Page</h2>

{{ Form::open(['url' => route('pages.store'), 'class' => 'form-horizontal']) }}

@include('pages._form')

{{ Form::close() }}

@endsection