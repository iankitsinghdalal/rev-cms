<div class="form-group">
    {{ Form::label('slug', 'Slug: ', []) }}
    {{ Form::text('slug', $page->slug ?? null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) }}
    @error('slug')
    <div class="form-control-feedback text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    {{ Form::button('Save', ['class' => 'btn btn-block btn-primary', 'type' => 'submit']) }}
</div>