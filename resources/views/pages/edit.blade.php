@extends('layouts.admin')

@section('content')

<h2 class="float-left">Update Page</h2>

{{ Form::open(['url' => route('pages.update', $page->id), 'class' => 'form-horizontal']) }}

@include('pages._form')

{{ Form::close() }}

@endsection