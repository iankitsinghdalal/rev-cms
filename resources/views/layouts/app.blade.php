<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/font-awesome/5.11.2/all.min.css') }}" rel="stylesheet" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick-carousel/1.8.1/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick-carousel/1.8.1/slick-theme.css') }}" rel="stylesheet">

    @yield('css')
</head>
<body class="bg-white">
    <div class="container-fluid d-flex flex-column min-vh-100">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/font-awesome/5.11.2/all.min.js') }}"></script>
    <script src="{{ asset('js/slick-carousel/1.8.1/slick.min.js') }}"></script>
    @yield('js')
</body>
</html>
