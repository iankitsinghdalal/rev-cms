<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img class="site-logo" src="{{ asset('images/logo.svg') }}" alt="Revinfotech Logo" width="200" height="42" />
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            @foreach(App\Services\MenuCRUDService::MENUS as $key => $menu)
            <li class="nav-item">
                <a class="nav-link
                    {{ Route::currentRouteName() }}
                    {{ strpos(Route::currentRouteName(), $menu['link']) !== false ? 'active' : null }}"
                    href="{{ route($menu['link']) }}"
                >
                {{ __("words.$menu[name]") }}
                </a>
            </li>
            @endforeach
            <li class="nav-item">
                {{ Form::open(['url' => route('set.locale')]) }}
                {{ Form::select('locale', [null => '--'] + array_column(App\Models\Language::where('status', 1)->get()->toArray(), 'short_name', 'short_name'), App::getLocale() ?? 'en', ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
                {{ Form::close() }}
            </li>
        </ul>
    </div>
</nav>