<?php

use App\Services\ShortCodeService;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('set-locale', 'HomeController@setLocale')->name('set.locale');

/**
 * Language Middleware
 */
Route::group(['middleware' => 'locale'], function() {

    /**
     * Public Routes
     */
    Route::get('/', function () {
        return (new ShortCodeService(view('welcome')))->execute();
    });

    /**
     * Bug
     * 
     *Route::get('{city}/{slug}', function ($city, $slug) {
     *   return view('public.slug')->with([
     *       'city' => $city, 'slug' => $slug
     *   ]);
     *});
     */

    /**
     * Routes accessible by authenticated users only
     */
    Route::group(['middleware' => 'auth'], function() {

        /**
         * Authorized usesrs with ROLE `guest`
         */
        Route::group(['prefix' => 'guest', 'middleware' => 'role:guest'], function() {
            Route::get('home', 'HomeController@guestIndex')->name('guest.home');
        });

        /**
         * Authorized usesrs with ROLE `user`
         */
        Route::group(['prefix' => 'user', 'middleware' => 'role:user'], function() {
            Route::get('home', 'HomeController@userIndex')->name('user.home');
        });

        Route::group(['prefix' => 'admin'], function() {

            /**
             * Authorized usesrs with ROLE `superadmin`
             */
            Route::group(['middleware' => 'role:superadmin'], function() {
                
                
            });

            /**
             * Authorized usesrs with ROLE `admin`
             */
            Route::group(['middleware' => 'role:admin'], function() {
                
                
            });

            /**
             * Authorized usesrs with ROLE `superadmin` OR `admin`
             */
            Route::group(['middleware' => ['role:superadmin|admin']], function() {

                Route::get('home', 'HomeController@adminIndex')->name('admin.home');
                Route::get('/profile', 'HomeController@adminProfile')->name('admin.profile');

                /** Company Information Route */    
                Route::post('/info-request','HomeController@adminInforRequest')->name('info-request');
                Route::post('/information-update','HomeController@InformationUpdate')->name('information-update');
                Route::post('/infodelete','HomeController@adminInfodelete');
            
                /** Social Link Route */ 
                Route::post('/social-request','HomeController@adminSocialLink')->name('social-request');
                Route::post('/social-update','HomeController@SocialUpdate')->name('social-update');
                Route::post('/socialdelete','HomeController@adminsocialdelete');
            
                /**
                 * Gallery route
                 */
                Route::group(['prefix' => 'gallery', 'namespace' => 'Admin'], function () {
                    Route::get('', 'GalleryController@index')->name('gallery');
                    Route::get('create', 'GalleryController@create')->name('gallery.create');
                    Route::post('store', 'GalleryController@store')->name('gallery.store')->middleware('optimizeImages');
                });

                /**
                 * Languages route
                 */
                Route::group(['prefix' => 'languages', 'namespace' => 'Admin'], function () {
                    Route::get('', 'LanguagesController@index')->name('languages');
                    Route::get('create', 'LanguagesController@create')->name('languages.create');
                    Route::post('store', 'LanguagesController@store')->name('languages.store');
                    Route::get('{id}/edit', 'LanguagesController@edit')->name('languages.edit');
                    Route::post('{id}/update', 'LanguagesController@update')->name('languages.update');
                    Route::delete('{id}/delete', 'LanguagesController@destroy')->name('languages.destroy');
                    Route::get('{id}/view', 'LanguagesController@show')->name('languages.show');
                });

                /**
                 * Menu Locations route
                 */
                Route::group(['prefix' => 'menu-location', 'namespace' => 'Admin'], function () {
                    Route::get('', 'MenuLocationController@index')->name('menu-location');
                    Route::get('create', 'MenuLocationController@create')->name('menu-location.create');
                    Route::post('store', 'MenuLocationController@store')->name('menu-location.store');
                    Route::get('{id}/edit', 'MenuLocationController@edit')->name('menu-location.edit');
                    Route::post('{id}/update', 'MenuLocationController@update')->name('menu-location.update');
                    Route::delete('{id}/delete', 'MenuLocationController@destroy')->name('menu-location.destroy');
                    Route::get('{id}/view', 'MenuLocationController@show')->name('menu-location.show');
                });

                /**
                 * Menus route
                 */
                Route::group(['prefix' => 'menus', 'namespace' => 'Admin'], function () {
                    Route::get('', 'MenusController@index')->name('menus');
                    Route::get('create', 'MenusController@create')->name('menus.create');
                    Route::post('store', 'MenusController@store')->name('menus.store');
                    Route::get('{id}/edit', 'MenusController@edit')->name('menus.edit');
                    Route::post('{id}/update', 'MenusController@update')->name('menus.update');
                    Route::delete('{id}/delete', 'MenusController@destroy')->name('menus.destroy');
                    Route::get('{id}/view', 'MenusController@show')->name('menus.show');
                });

                /**
                 * SubMenus route
                 */
                Route::group(['prefix' => 'sub-menus', 'namespace' => 'Admin'], function () {
                    Route::get('', 'SubMenusController@index')->name('sub-menus');
                    Route::get('create', 'SubMenusController@create')->name('sub-menus.create');
                    Route::post('store', 'SubMenusController@store')->name('sub-menus.store');
                    Route::get('{id}/edit', 'SubMenusController@edit')->name('sub-menus.edit');
                    Route::post('{id}/update', 'SubMenusController@update')->name('sub-menus.update');
                    Route::delete('{id}/delete', 'SubMenusController@destroy')->name('sub-menus.destroy');
                    Route::get('{id}/view', 'SubMenusController@show')->name('sub-menus.show');
                });

                /**
                 * Partners route
                 */
                Route::group(['prefix' => 'partners', 'namespace' => 'Admin'], function () {
                    Route::get('', 'PartnersController@index')->name('partners');
                    Route::get('create', 'PartnersController@create')->name('partners.create');
                    Route::post('store', 'PartnersController@store')->name('partners.store');
                    Route::get('{id}/edit', 'PartnersController@edit')->name('partners.edit');
                    Route::post('{id}/update', 'PartnersController@update')->name('partners.update');
                    Route::delete('{id}/delete', 'PartnersController@destroy')->name('partners.destroy');
                    Route::get('{id}/view', 'PartnersController@show')->name('partners.show');
                });
            
                /**
                 * Clients route
                 */
                Route::group(['prefix' => 'clients', 'namespace' => 'Admin'], function () {
                    Route::get('', 'ClientsController@index')->name('clients');
                    Route::get('create', 'ClientsController@create')->name('clients.create');
                    Route::post('store', 'ClientsController@store')->name('clients.store');
                    Route::get('{id}/edit', 'ClientsController@edit')->name('clients.edit');
                    Route::post('{id}/update', 'ClientsController@update')->name('clients.update');
                    Route::delete('{id}/delete', 'ClientsController@destroy')->name('clients.destroy');
                    Route::get('{id}/view', 'ClientsController@show')->name('clients.show');
                });

                /**
                 * Industries route
                 */
                Route::group(['prefix' => 'industries', 'namespace' => 'Admin'], function () {
                    Route::get('', 'IndustriesController@index')->name('industries');
                    Route::get('create', 'IndustriesController@create')->name('industries.create');
                    Route::post('store', 'IndustriesController@store')->name('industries.store');
                    Route::get('{id}/edit', 'IndustriesController@edit')->name('industries.edit');
                    Route::post('{id}/update', 'IndustriesController@update')->name('industries.update');
                    Route::delete('{id}/delete', 'IndustriesController@destroy')->name('industries.destroy');
                    Route::get('{id}/view', 'IndustriesController@show')->name('industries.show');
                });

                /**
                 * Portfolios route
                 */
                Route::group(['prefix' => 'portfolios', 'namespace' => 'Admin'], function () {
                    Route::get('', 'PortfoliosController@index')->name('portfolios');
                    Route::get('create', 'PortfoliosController@create')->name('portfolios.create');
                    Route::post('store', 'PortfoliosController@store')->name('portfolios.store');
                    Route::get('{id}/edit', 'PortfoliosController@edit')->name('portfolios.edit');
                    Route::post('{id}/update', 'PortfoliosController@update')->name('portfolios.update');
                    Route::delete('{id}/delete', 'PortfoliosController@destroy')->name('portfolios.destroy');
                    Route::get('{id}/view', 'PortfoliosController@show')->name('portfolios.show');
                });

                /**
                 * Team route
                 */
                Route::group(['prefix' => 'team', 'namespace' => 'Admin'], function () {
                    Route::get('', 'TeamController@index')->name('team');
                    Route::get('create', 'TeamController@create')->name('team.create');
                    Route::post('store', 'TeamController@store')->name('team.store');
                    Route::get('{id}/edit', 'TeamController@edit')->name('team.edit');
                    Route::post('{id}/update', 'TeamController@update')->name('team.update');
                    Route::delete('{id}/delete', 'TeamController@destroy')->name('team.destroy');
                    Route::get('{id}/view', 'TeamController@show')->name('team.show');
                });

                /**
                 * Awards route
                 */
                Route::group(['prefix' => 'awards', 'namespace' => 'Admin'], function () {
                    Route::get('', 'AwardsController@index')->name('awards');
                    Route::get('create', 'AwardsController@create')->name('awards.create');
                    Route::post('store', 'AwardsController@store')->name('awards.store');
                    Route::get('{id}/edit', 'AwardsController@edit')->name('awards.edit');
                    Route::post('{id}/update', 'AwardsController@update')->name('awards.update');
                    Route::delete('{id}/delete', 'AwardsController@destroy')->name('awards.destroy');
                    Route::get('{id}/view', 'AwardsController@show')->name('awards.show');
                });

                /**
                 * Case Studies route
                 */
                Route::group(['prefix' => 'case-studies', 'namespace' => 'Admin'], function () {
                    Route::get('', 'CaseStudiesController@index')->name('case-studies');
                    Route::get('create', 'CaseStudiesController@create')->name('case-studies.create');
                    Route::post('store', 'CaseStudiesController@store')->name('case-studies.store');
                    Route::get('{id}/edit', 'CaseStudiesController@edit')->name('case-studies.edit');
                    Route::post('{id}/update', 'CaseStudiesController@update')->name('case-studies.update');
                    Route::delete('{id}/delete', 'CaseStudiesController@destroy')->name('case-studies.destroy');
                    Route::get('{id}/view', 'CaseStudiesController@show')->name('case-studies.show');
                });

                /**
                 * Case Studies route
                 */
                Route::group(['prefix' => 'blogs', 'namespace' => 'Admin'], function () {
                    Route::get('', 'BlogsController@index')->name('blogs');
                    Route::get('create', 'BlogsController@create')->name('blogs.create');
                    Route::post('store', 'BlogsController@store')->name('blogs.store');
                    Route::get('{id}/edit', 'BlogsController@edit')->name('blogs.edit');
                    Route::post('{id}/update', 'BlogsController@update')->name('blogs.update');
                    Route::delete('{id}/delete', 'BlogsController@destroy')->name('blogs.destroy');
                    Route::get('{id}/view', 'BlogsController@show')->name('blogs.show');
                });

                /**
                 * Job Openings route
                 */
                Route::group(['prefix' => 'job-openings', 'namespace' => 'Admin'], function () {
                    Route::get('', 'JobOpeningsController@index')->name('job-openings');
                    Route::get('create', 'JobOpeningsController@create')->name('job-openings.create');
                    Route::post('store', 'JobOpeningsController@store')->name('job-openings.store');
                    Route::get('{id}/edit', 'JobOpeningsController@edit')->name('job-openings.edit');
                    Route::post('{id}/update', 'JobOpeningsController@update')->name('job-openings.update');
                    Route::delete('{id}/delete', 'JobOpeningsController@destroy')->name('job-openings.destroy');
                    Route::get('{id}/view', 'JobOpeningsController@show')->name('job-openings.show');
                });

                /**
                 * Pages route
                 */
                Route::group(['prefix' => 'pages', 'namespace' => 'Admin'], function () {
                    Route::get('', 'PagesController@index')->name('pages');
                    Route::get('create', 'PagesController@create')->name('pages.create');
                    Route::post('store', 'PagesController@store')->name('pages.store');
                    Route::get('{id}/edit', 'PagesController@edit')->name('pages.edit');
                    Route::post('{id}/update', 'PagesController@update')->name('pages.update');
                    Route::delete('{id}/delete', 'PagesController@destroy')->name('pages.destroy');
                    Route::get('{id}/view', 'PagesController@show')->name('pages.show');
                });
            });
        });
    });
});