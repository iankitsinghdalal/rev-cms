## Get the language package work
1. Add supported languages excepts `en` under `SUPPORTED_LANGUAGES` into the `.env` file comma-seperated (short name(s) only).
1. Add all translations key:value pair into the /resources/lang/en/ directory.
2. None of the entries into the file(s) should contain blank array.
3. run `php artisan autotrans:all` to regenerate language translation file(s) for all supported languages. Note:: It will overwrite already existing key:value pair(s)
4. OR run `php artisan autotrans:missing` to generate key:value pair of only missing entries for all supported languages.
