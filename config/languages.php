<?php

return [

    /*
    |--------------------------------------------------------------------------
    | List of all supported languages by your application
    |--------------------------------------------------------------------------
    |
    */

    'en' => 'English',
    'fr' => 'French',
    'hi' => 'Hindi',
    'du' => 'Dutch'

];
