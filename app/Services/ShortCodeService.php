<?php

namespace App\Services;

use App\Models\Team;
use App\Models\Award;
use App\Models\Industry;
use App\Models\Partner;
use App\Models\Client;

class ShortCodeService
{
    /**
     * 
     */
    private $_snippet;
    private $_callable;

    /**
     * 
     */
    public function __construct($snippet)
    {
        $this->_snippet = $snippet;
    }

    /**
     * 
     */
    public function execute()
    {
        // Check if string contains shortcode
        preg_match_all('/\[(.*?)\]/', $this->_snippet, $matches);
        $matches = array_pop($matches);

        // Loop to check for multiple functions
        foreach($matches as $match) {
            // Get name from list
            $splitItems = explode(" ", $match);
            $splitFunc = $splitItems[0];
            array_shift($splitItems);
            
            // Get function arguments from list
            $funcArgs = [];
            foreach ($splitItems as $items) {
                $item = explode("=", $items);
                $funcArgs[$item[0]] = $item[1];
            }
    
            // Check function exists in shortcode
            if(($this->_callable = method_exists(self::class, $splitFunc) ? $splitFunc : null)) {
                // Execute the function and pass on the arguments to the function
                $response = call_user_func_array(array($this, $this->_callable), $funcArgs);
            
                // Merge the response to the provided snippet
                $this->_snippet = str_replace("[".$match."]", $response, $this->_snippet);
            }
        }

        // Return the snippet after making the changes through the shortcode
        return $this->_snippet;
    }

    /**
     * 
     */
    private function services($lang, $length, $sort)
    {
        return "<span style='color:red'>Services</span> will be returned in ".$lang." with ".$length." records with ".$sort." order";
    }
    
    /**
     * 
     */
    private function team($lang, $length, $sort)
    {
        $team = Team::whereHas('language', function ($query) use ($lang) {
            $query->where('short_name', $lang);
        })->orderby('id', $sort)->limit($length)->get();

        return view('partials.short-codes._team', compact('team'));
    }

    /**
     * 
     */
    private function industries($lang, $length, $sort)
    {
        $industries = Industry::whereHas('language', function ($query) use ($lang) {
            $query->where('short_name', $lang);
        })->orderby('id', $sort)->limit($length)->get();

        return view('partials.short-codes._industries', compact('industries'));
    }

    private function awards($lang, $length, $sort)
    {
        $awards = Award::whereHas('language', function ($query) use ($lang) {
            $query->where('short_name', $lang);
        })->orderby('id', $sort)->limit($length)->get();

        return view('partials.short-codes._awards', compact('awards'));
    }

    private function partners($lang, $length, $sort)
    {
        $partners = Partner::whereHas('language', function ($query) use ($lang) {
            $query->where('short_name', $lang);
        })->orderby('id', $sort)->limit($length)->get();

        return view('partials.short-codes._partners', compact('partners'));
    }

    private function clients($lang, $length, $sort)
    {
        $clients = Client::whereHas('language', function ($query) use ($lang) {
            $query->where('short_name', $lang);
        })->orderby('id', $sort)->limit($length)->get();

        return view('partials.short-codes._clients', compact('clients'));
    }
}