<?php

namespace App\Services;

class MenuCRUDService
{
    const MENUS = [
        ['name' => 'Languages', 'link' => 'languages'],
        ['name' => 'Menu Location', 'link' => 'menu-location'],
        ['name' => 'Menus', 'link' => 'menus'],
        ['name' => 'Sub Menus', 'link' => 'sub-menus'],
        ['name' => 'Partners', 'link' => 'partners'],
        ['name' => 'Clients', 'link' => 'clients'],
        ['name' => 'Portfolios', 'link' => 'portfolios'],
        ['name' => 'Team', 'link' => 'team'],
        ['name' => 'Awards', 'link' => 'awards'],
        ['name' => 'Case Studies', 'link' => 'case-studies'],
        ['name' => 'Blogs', 'link' => 'blogs'],
        ['name' => 'Job Openings', 'link' => 'job-openings'],
        ['name' => 'Gallery', 'link' => 'gallery']
    ];

    public function getMenus() {
        return self::MENUS;
    }
}
