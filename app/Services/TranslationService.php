<?php

namespace App\Services;

use Stichoza\GoogleTranslate\GoogleTranslate;

class TranslationService
{
    private $_string;
    private $_translation;

    public function __construct($string, $to = 'en', $from = 'en') {
        $this->_string = $string;
        $this->translate($to, $from);
    }

    private function translate($to, $from) {
        if ($to === $from) {
            $this->_translation = $this->_string;
            return;
        }
        $this->_translation = (new GoogleTranslate)
                                ->setSource($from)
                                ->setTarget($to)
                                ->translate($this->_string);
        return;
    }

    public function getTranslatedString() {
        return $this->_translation;
    }
}
