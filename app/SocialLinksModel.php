<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLinksModel extends Model
{
    protected $table = "social_link";
    protected $fillable = ['social','url','image'];
}
