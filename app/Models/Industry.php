<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'industries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'fa_icon', 'language_id', 'status', 'order'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'language_id' => 1
    ];

    /**
     * 
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
        static::updating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
    }

    /**
     * 
     */
    public function getReadableStatusAttribute()
    {
        return $this->status ? 'Active' : 'Inactive';
    }

    /**
     * 
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}
