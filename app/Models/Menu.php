<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'location_id', 'language_id', 'status', 'order'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'language_id' => 1
    ];

    /**
     * 
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
        static::updating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
    }

    /**
     * 
     */
    public function getReadableStatusAttribute()
    {
        return $this->status ? 'Active' : 'Inactive';
    }

    /**
     * 
     */
    public function location()
    {
        return $this->belongsTo(MenuLocation::class, 'location_id', 'id');
    }

    /**
     * 
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }

    /**
     * 
     */
    public function subMenus()
    {
        return $this->hasMany(SubMenu::class, 'menu_id', 'id');
    }
}
