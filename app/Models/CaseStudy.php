<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaseStudy extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'case_studies';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'banner_image', 'slug', 'title', 'meta_title', 'meta_description', 'keywords', 'description', /*'category_id',*/ 'author_name', 'language_id', 'status', 'order'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'language_id' => 1
    ];

    /**
     * 
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
        static::updating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
    }

    /**
     * 
     */
    public function fImage()
    {
        return $this->belongsTo(Gallery::class, 'image', 'id');
    }

    /**
     * 
     */
    public function fBannerImage()
    {
        return $this->belongsTo(Gallery::class, 'banner_image', 'id');
    }

    /**
     * 
     */
    // public function category()
    // {
    //     return null;
    // }

    /**
     * It must be replaced with category relation
     */
    public function getCategoryAttribute()
    {
        return null;
    }

    /**
     * 
     */
    public function getReadableStatusAttribute()
    {
        return $this->status ? 'Active' : 'Inactive';
    }

    /**
     * 
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}
