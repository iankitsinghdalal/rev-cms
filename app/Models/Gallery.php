<?php

namespace App\Models;

use Storage;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'path', 'auth_by', 'size'
    ];

    /**
     * The attribute and list the fields that would automatically be appended.
     *
     * @var array
     */
    protected $appends = [
        'url', 'uploaded_time', 'size_in_kb'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'size' => 0
    ];
    
    public function getUrlAttribute()
    {
        //Storage::disk('s3')->url($this->path);
        return $this->path;
    }

    public function getUploadedTimeAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'auth_by');
    }

    public function getSizeInKbAttribute()
    {
        return round($this->size / 1024, 2);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($image) {
            $image->auth_by = auth()->user()->id;
        });
    }
}
