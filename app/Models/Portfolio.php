<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'portfolios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'logo', 'feature_image', 'normal_images', 'description', 'language_id', 'status', 'order'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'normal_images' => 'array'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'language_id' => 1
    ];

    /**
     * 
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
        static::updating(function ($model) {
            $model->order = $model->order ?? 1;
            $model->status = $model->status ?? 0;
        });
    }

    /**
     * 
     */
    public function setNormalImagesAttribute($value)
    {
        $this->attributes['normal_images'] = json_encode($value);
    }

    /**
     * 
     */
    public function fLogo()
    {
        return $this->belongsTo(Gallery::class, 'logo', 'id');
    }

    /**
     * 
     */
    public function fFeatureImage()
    {
        return $this->belongsTo(Gallery::class, 'feature_image', 'id');
    }

    /**
     * 
     */
    public function getReadableStatusAttribute()
    {
        return $this->status ? 'Active' : 'Inactive';
    }

    /**
     * 
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}
