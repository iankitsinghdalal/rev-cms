<?php

namespace App\Http\Requests\SubMenus;

use App\Rules\CombineUnique;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class StoreSubMenu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:225|uniqueOfMultiple:sub_menus,menu_id-' . $this->get('menu_id', 'NULL'),
            'menu_id' => 'required|exists:menus,id',
            'order' => 'digits_between:0,10'
        ];
    }

    /**
     * Custom error messages
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'This field is required.',
            'exists' => 'Please choose a valid option.'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('phone_unique', function ($attribute, $value, $parameters, $validator) {
            $inputs = $validator->getData();
            $code = $inputs['code'];
            $phone = $inputs['phone'];
            $concatenated_number = $code . ' ' . $phone;
            $except_id = (!empty($parameters)) ? head($parameters) : null;

            $query = User::where('phone', $concatenated_number);
            if(!empty($except_id)) {
            $query->where('id', '<>', $except);
            }

            return $query->exists();
        });
    }
}
