<?php

namespace App\Http\Requests\SubMenus;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubMenu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * Need to write custom validator to make `menu_id + name` unique
         */
        return [
            'name' => 'required|max:225|uniqueOfMultiple:sub_menus,menu_id-' . $this->get('menu_id', 'NULL') . ',except-id-' . $this->route('id', 'NULL'),
            'menu_id' => 'required|exists:menus,id',
            'order' => 'digits_between:0,10'
        ];
    }

    /**
     * Custom error messages
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'This field is required.',
            'exists' => 'Please choose a valid option.'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
}
