<?php

namespace App\Http\Requests\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|exists:gallery,id',
            'banner_image' => 'required|exists:gallery,id',
            'slug' => 'required|max:225|unique:case_studies',
            'status' => 'required|min:0|max:1',
            'title' => 'required|max:225',
            'meta_title' => 'required|max:225',
            'meta_description' => 'required|max:1000',
            'keywords' => 'required|max:225',
            'description' => 'required|max:1500',
            'category_id' => 'nullable',
            'author_name' => 'required|max:225',
            'author_image' => 'required|exists:gallery,id',
            'author_content' => 'required|max:1500',
        ];
    }

    /**
     * Custom error messages
     * 
     * @return array
     */
    public function messages()
    {
        return parent::messages();
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
}
