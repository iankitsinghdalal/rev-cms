<?php

namespace App\Http\Requests\Portfolios;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePortfolio extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'required|exists:gallery,id',
            'feature_image' => 'required|exists:gallery,id',
            'normal_images' => 'required|array|min:1',
            'normal_images.*' => 'required|exists:gallery,id|distinct',
            'description' => 'required|max:1000'
        ];
    }

    /**
     * Custom error messages
     * 
     * @return array
     */
    public function messages()
    {
        return parent::messages();
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
}
