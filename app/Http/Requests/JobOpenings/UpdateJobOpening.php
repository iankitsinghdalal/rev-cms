<?php

namespace App\Http\Requests\JobOpenings;

use Illuminate\Foundation\Http\FormRequest;

class UpdateJobOpening extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'required|max:225',
            'department' => 'required|max:225',
            'experience_required' => 'required|max:225',
            'location' => 'required|max:225',
            'sharp_skills' => 'required|max:225',
            'skills' => 'required|max:225',
            'job_description' => 'required|max:1500',
            'remarks' => 'max:1000'
        ];
    }

    /**
     * Custom error messages
     * 
     * @return array
     */
    public function messages()
    {
        return parent::messages();
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
}
