<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Providers\RouteServiceProvider;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (auth()->guard($guard)->check()) {
            if(in_array(auth()->user()->role, [User::ROLE_ADMIN, User::ROLE_SUPERADMIN])) {
                return redirect()->route('admin.home');
            } elseif (auth()->user()->role === User::ROLE_USER) {
                return redirect()->route('user.home');
            } else {
                return redirect()->route('guest.home');
            }
        }

        return $next($request);
    }
}
