<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        abort_unless(auth()->check() && in_array(auth()->user()->role, explode('|', $role)), 403, "You don't have permissions to access this area");
        return $next($request);
    }
}
