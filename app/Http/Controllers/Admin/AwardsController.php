<?php

namespace App\Http\Controllers\Admin;

use App\Models\Award;
use App\Models\Gallery;
use App\Models\Language;
use App\Services\TranslationService;
use App\Http\Requests\Awards\StoreAward;
use App\Http\Requests\Awards\UpdateAward;
use App\Http\Controllers\Controller as BaseController; 

class AwardsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('awards.index')->with([
            'awards' => Award::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('awards.create')->with([
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Awards\StoreAward  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAward $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'title' => (new TranslationService($request->input('title'), $value->short_name, 'en'))->getTranslatedString(),
                'description' => (new TranslationService($request->input('description'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Award::create($request->only('image', 'title', 'description', 'language_id', 'status', 'order'));
        }
        return redirect(route('awards'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Award  $award
     * @return \Illuminate\Http\Response
     */
    public function show(Award $award)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('awards.edit')->with([
            'award' => Award::findOrFail($id),
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Awards\UpdateAward  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAward $request, $id)
    {
        Award::findOrFail($id)->update($request->only('image', 'title', 'description', 'status', 'order'));
        return redirect(route('awards'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Award::findOrFail($id)->delete();
        return redirect(route('awards'));
    }
}
