<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Models\SubMenu;
use App\Services\TranslationService;
use App\Http\Requests\SubMenus\StoreSubMenu;
use App\Http\Requests\SubMenus\UpdateSubMenu;
use App\Http\Controllers\Controller as BaseController;

class SubMenusController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sub-menus.index')->with([
            'subMenus' => SubMenu::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sub-menus.create')->with([
            'menus' => array_column(Menu::select(['id', 'name'])->get()->toArray(), 'name', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\SubMenus\StoreSubMenu  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubMenu $request)
    {
        $menu = Menu::findOrFail($request->input('menu_id'));

        $request->merge([
            'name' => (new TranslationService($request->input('name'), $menu->language->short_name, 'en'))->getTranslatedString()
        ]);

        $menu->subMenus()->create($request->only('name', 'status', 'order'));
        return redirect(route('sub-menus'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('sub-menus.edit')->with([
            'subMenu' => SubMenu::findOrFail($id),
            'menus' => array_column(Menu::all()->toArray(), 'name', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\SubMenus\UpdateSubMenu  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubMenu $request, $id)
    {
        SubMenu::findOrFail($id)->update($request->only('name', 'menu_id', 'status', 'order'));
        return redirect(route('sub-menus'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubMenu::findOrFail($id)->delete();
        return redirect(route('sub-menus'));
    }
}
