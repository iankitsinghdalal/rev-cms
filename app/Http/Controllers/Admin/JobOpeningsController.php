<?php

namespace App\Http\Controllers\Admin;

use App\Models\Language;
use App\Models\JobOpening;
use App\Services\TranslationService;
use App\Http\Requests\JobOpenings\StoreJobOpening;
use App\Http\Requests\JobOpenings\UpdateJobOpening;
use App\Http\Controllers\Controller as BaseController;

class JobOpeningsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('job-openings.index')->with([
            'jobOpenings' => JobOpening::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('job-openings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\JobOpenings\StoreJobOpening  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJobOpening $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'position' => (new TranslationService($request->input('position'), $value->short_name, 'en'))->getTranslatedString(),
                'department' => (new TranslationService($request->input('department'), $value->short_name, 'en'))->getTranslatedString(),
                'experience_required' => (new TranslationService($request->input('experience_required'), $value->short_name, 'en'))->getTranslatedString(),
                'location' => (new TranslationService($request->input('location'), $value->short_name, 'en'))->getTranslatedString(),
                'sharp_skills' => (new TranslationService($request->input('sharp_skills'), $value->short_name, 'en'))->getTranslatedString(),
                'skills' => (new TranslationService($request->input('skills'), $value->short_name, 'en'))->getTranslatedString(),
                'job_description' => (new TranslationService($request->input('job_description'), $value->short_name, 'en'))->getTranslatedString(),
                'remarks' => (new TranslationService($request->input('remarks'), $value->short_name, 'en'))->getTranslatedString(),

            ]);
            JobOpening::create($request->only('position', 'department', 'experience_required', 'location', 'sharp_skills', 'skills', 'job_description', 'remarks', 'language_id', 'status', 'order'));
        }
        return redirect(route('job-openings'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobOpenings  $jobOpenings
     * @return \Illuminate\Http\Response
     */
    public function show(JobOpenings $jobOpenings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('job-openings.edit')->with([
            'jobOpening' => JobOpening::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\JobOpenings\UpdateJobOpening  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJobOpening $request, $id)
    {
        JobOpening::findOrFail($id)->update($request->only('position', 'department', 'experience_required', 'location', 'sharp_skills', 'skills', 'job_description', 'remarks', 'status', 'order'));
        return redirect(route('job-openings'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JobOpening::findOrFail($id)->delete();
        return redirect(route('job-openings'));
    }
}
