<?php

namespace App\Http\Controllers\Admin;

use App\Models\Team;
use App\Models\Gallery;
use App\Models\Language;
use App\Services\TranslationService;
use App\Http\Requests\Team\StoreTeam;
use App\Http\Requests\Team\UpdateTeam;
use App\Http\Controllers\Controller as BaseController;

class TeamController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('team.index')->with([
            'team' => Team::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team.create')->with([
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Team\StoreTeam $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeam $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'name' => (new TranslationService($request->input('name'), $value->short_name, 'en'))->getTranslatedString(),
                'description' => (new TranslationService($request->input('description'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Team::create($request->only('image', 'name', 'description', 'language_id', 'status', 'order'));
        }
        return redirect(route('team'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('team.edit')->with([
            'member' => Team::findOrFail($id),
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Team\UpdateTeam $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeam $request, $id)
    {
        Team::findOrFail($id)->update($request->only('image', 'name', 'description', 'status', 'order'));
        return redirect(route('team'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::findOrFail($id)->delete();
        return redirect(route('team'));
    }
}
