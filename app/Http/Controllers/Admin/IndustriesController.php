<?php

namespace App\Http\Controllers\Admin;

use App\Models\Industry;
use App\Models\Language;
use App\Services\TranslationService;
use App\Http\Requests\Industries\StoreIndustry;
use App\Http\Requests\Industries\UpdateIndustry;
use App\Http\Controllers\Controller as BaseController;

class IndustriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('industries.index')->with([
            'industries' => Industry::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('industries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Industries\StoreIndustry  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIndustry $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'title' => (new TranslationService($request->input('title'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Industry::create($request->only('title', 'fa_icon', 'language_id', 'status', 'order'));
        }
        return redirect(route('industries'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function show(Industry $industry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('industries.edit')->with([
            'industry' => Industry::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Industries\UpdateIndustry  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateIndustry $request, $id)
    {
        Industry::findOrFail($id)->update($request->only('title', 'fa_icon', 'status', 'order'));
        return redirect(route('industries'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Industry::findOrFail($id)->delete();
        return redirect(route('industries'));
    }
}
