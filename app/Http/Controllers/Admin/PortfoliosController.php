<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\Language;
use App\Models\Portfolio;
use App\Services\TranslationService;
use App\Http\Requests\Portfolios\StorePortfolio;
use App\Http\Requests\Portfolios\UpdatePortfolio;
use App\Http\Controllers\Controller as BaseController;

class PortfoliosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portfolios.index')->with([
            'portfolios' => Portfolio::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('portfolios.create')->with([
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Portfolios\StorePortfolio $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePortfolio $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'description' => (new TranslationService($request->input('description'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Portfolio::create($request->only('logo', 'feature_image', 'normal_images', 'description', 'language_id', 'status', 'order'));
        }
        return redirect(route('portfolios'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('portfolios.edit')->with([
            'portfolio' => Portfolio::findOrFail($id),
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Portfolios\UpdatePortfolio $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePortfolio $request, $id)
    {
        Portfolio::findOrFail($id)->update($request->only('logo', 'feature_image', 'normal_images', 'description', 'status', 'order'));
        return redirect(route('portfolios'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Portfolio::findOrFail($id)->delete();
        return redirect(route('portfolios'));
    }
}
