<?php

namespace App\Http\Controllers\Admin;

use App\Models\Partner;
use App\Models\Gallery;
use App\Models\Language;
use App\Services\TranslationService;
use App\Http\Requests\Partners\StorePartner;
use App\Http\Requests\Partners\UpdatePartner;
use App\Http\Controllers\Controller as BaseController;

class PartnersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partners.index')->with([
            'partners' => Partner::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partners.create')->with([
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Partners\StorePartner  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePartner $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'title' => (new TranslationService($request->input('title'), $value->short_name, 'en'))->getTranslatedString(),
                'description' => (new TranslationService($request->input('description'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Partner::create($request->only('image', 'title', 'description', 'language_id', 'status', 'order'));
        }
        return redirect(route('partners'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('partners.edit')->with([
            'partner' => Partner::findOrFail($id),
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Partners\UpdatePartner  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePartner $request, $id)
    {
        Partner::findOrFail($id)->update($request->only('image', 'title', 'description', 'status', 'order'));
        return redirect(route('partners'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Partner::findOrFail($id)->delete();
        return redirect(route('partners'));
    }
}
