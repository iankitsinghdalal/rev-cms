<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Gallery;
use App\Models\Language;
use App\Services\TranslationService;
use App\Http\Requests\Clients\StoreClient;
use App\Http\Requests\Clients\UpdateClient;
use App\Http\Controllers\Controller as BaseController;

class ClientsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clients.index')->with([
            'clients' => Client::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create')->with([
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Partners\StoreClient  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClient $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'title' => (new TranslationService($request->input('title'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Client::create($request->only('image', 'title', 'language_id', 'status', 'order'));
        }
        return redirect(route('clients'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('clients.edit')->with([
            'client' => Client::findOrFail($id),
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Partners\UpdateClient  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClient $request, $id)
    {
        Client::findOrFail($id)->update($request->only('image', 'title', 'status', 'order'));
        return redirect(route('clients'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::findOrFail($id)->delete();
        return redirect(route('clients'));
    }
}
