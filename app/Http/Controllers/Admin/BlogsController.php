<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blog;
use App\Models\Gallery;
use App\Models\Language;
use App\Services\TranslationService;
use App\Http\Requests\Blogs\StoreBlog;
use App\Http\Requests\Blogs\UpdateBlog;
use App\Http\Controllers\Controller as BaseController;

class BlogsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('blogs.index')->with([
            'blogs' => Blog::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.create')->with([
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Blogs\StoreBlog  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlog $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'title' => (new TranslationService($request->input('title'), $value->short_name, 'en'))->getTranslatedString(),
                'description' => (new TranslationService($request->input('description'), $value->short_name, 'en'))->getTranslatedString(),
                'author_name' => (new TranslationService($request->input('author_name'), $value->short_name, 'en'))->getTranslatedString(),
                'author_content' => (new TranslationService($request->input('author_content'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Blog::create($request->only('image', 'banner_image', 'slug', 'title', 'meta_title', 'meta_description', 'keywords', 'description', 'category_id', 'author_name', 'author_image', 'author_content', 'language_id', 'status', 'order'));
        }
        return redirect(route('blogs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('blogs.edit')->with([
            'blog' => Blog::findOrFail($id),
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Blogs\UpdateBlog  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlog $request, $id)
    {
        Blog::findOrFail($id)->update($request->only('image', 'banner_image', 'slug', 'status', 'title', 'meta_title', 'meta_description', 'keywords', 'description', 'category_id', 'author_name', 'author_image', 'author_content', 'status', 'order'));
        return redirect(route('blogs'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::findOrFail($id)->delete();
        return redirect(route('blogs'));
    }
}
