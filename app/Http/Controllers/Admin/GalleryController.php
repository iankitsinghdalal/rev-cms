<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Gallery\StoreImage;
use App\Http\Controllers\Controller as BaseController;

class GalleryController extends BaseController
{
    /**
     * 
     */
    private $image;

    /**
     * 
     */
    public function __construct(Gallery $image)
    {
        $this->image = $image;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('gallery.index')->with([
            'gallery' => Gallery::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Gallery\StoreImage  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreImage $request)
    {
        $path = Storage::disk('s3')->put('gallery', $request->file);
        $request->merge([
            'size' => $request->file->getClientSize(),
            'path' => Storage::disk('s3')->url($path)
        ]);
        $this->image->create($request->only('path', 'title', 'size'));
        return redirect(route('gallery'));
    }
}
