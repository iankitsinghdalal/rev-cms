<?php

namespace App\Http\Controllers\Admin;

use App\Models\MenuLocation;
use Illuminate\Http\Request;
use App\Http\Requests\MenuLocation\StoreMenuLocation;
use App\Http\Requests\MenuLocation\UpdateMenuLocation;
use App\Http\Controllers\Controller as BaseController;


class MenuLocationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menu-location.index')->with([
            'menuLocations' => MenuLocation::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu-location.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\MenuLocations\StoreMenuLocation $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenuLocation $request)
    {
        MenuLocation::create($request->only('name', 'status'));
        return redirect(route('menu-location'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('menu-location.edit')->with([
            'menuLocation' => MenuLocation::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\MenuLocations\UpdateMenuLocation  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenuLocation $request, $id)
    {
        MenuLocation::findOrFail($id)->update($request->only('name', 'status'));
        return redirect(route('menu-location'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MenuLocation::findOrFail($id)->delete();
        return redirect(route('menu-location'));
    }
}
