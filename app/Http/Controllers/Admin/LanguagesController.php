<?php

namespace App\Http\Controllers\Admin;

use App\Models\Language;
use App\Http\Requests\Languages\StoreLanguage;
use App\Http\Requests\Languages\UpdateLanguage;
use App\Http\Controllers\Controller as BaseController;

class LanguagesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('languages.index')->with([
            'languages' => Language::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Languages\StoreLanguage $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLanguage $request)
    {
        Language::create($request->only('name', 'short_name', 'status'));
        return redirect(route('languages'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('languages.edit')->with([
            'language' => Language::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Languages\UpdateLanguage  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLanguage $request, $id)
    {
        Language::findOrFail($id)->update($request->only('name', 'short_name', 'status'));
        return redirect(route('languages'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Language::findOrFail($id)->delete();
        return redirect(route('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function feedModal()
    {
        return view('languages.create');
    }
}
