<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Models\Language;
use App\Models\MenuLocation;
use App\Services\TranslationService;
use App\Http\Requests\Menus\StoreMenu;
use App\Http\Requests\Menus\UpdateMenu;
use App\Http\Controllers\Controller as BaseController;

class MenusController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menus.index')->with([
            'menus' => Menu::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menus.create')->with([
            'menuLocations' => array_column(MenuLocation::all()->toArray(), 'name', 'id'),

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Menus\StoreMenu  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenu $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'name' => (new TranslationService($request->input('name'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            Menu::create($request->only('name', 'location_id', 'language_id', 'status', 'order'));
        }
        return redirect(route('menus'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('menus.edit')->with([
            'menu' => Menu::findOrFail($id),
            'menuLocations' => array_column(MenuLocation::all()->toArray(), 'name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Menus\UpdateMenu  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenu $request, $id)
    {
        Menu::findOrFail($id)->update($request->only('name', 'location_id', 'status', 'order'));
        return redirect(route('menus'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::findOrFail($id)->delete();
        return redirect(route('menus'));
    }
}
