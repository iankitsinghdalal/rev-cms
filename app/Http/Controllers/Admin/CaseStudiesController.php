<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\Language;
use App\Models\CaseStudy;
use App\Services\TranslationService;
use App\Http\Requests\CaseStudies\StoreCaseStudy;
use App\Http\Requests\CaseStudies\UpdateCaseStudy;
use App\Http\Controllers\Controller as BaseController;

class CaseStudiesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('case-studies.index')->with([
            'caseStudies' => CaseStudy::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('case-studies.create')->with([
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CaseStudies\StoreCaseStudy  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCaseStudy $request)
    {
        foreach(Language::all() as $key => $value) {
            $request->merge([
                'language_id' => $value->id,
                'title' => (new TranslationService($request->input('title'), $value->short_name, 'en'))->getTranslatedString(),
                'description' => (new TranslationService($request->input('description'), $value->short_name, 'en'))->getTranslatedString(),
                'author_name' => (new TranslationService($request->input('author_name'), $value->short_name, 'en'))->getTranslatedString()
            ]);
            CaseStudy::create($request->only('image', 'banner_image', 'slug', 'title', 'meta_title', 'meta_description', 'keywords', 'description', 'category_id', 'author_name', 'language_id', 'status', 'order'));
        }
        return redirect(route('case-studies'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CaseStudy  $caseStudy
     * @return \Illuminate\Http\Response
     */
    public function show(CaseStudy $caseStudy)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('case-studies.edit')->with([
            'caseStudy' => CaseStudy::findOrFail($id),
            'gallery' => array_column(Gallery::all()->toArray(), 'path', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\CaseStudies\UpdateCaseStudy  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCaseStudy $request, $id)
    {
        CaseStudy::findOrFail($id)->update($request->only('image', 'banner_image', 'slug', 'title', 'meta_title', 'meta_description', 'keywords', 'description', 'category_id', 'author_name', 'status', 'order'));
        return redirect(route('case-studies'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CaseStudy::findOrFail($id)->delete();
        return redirect(route('case-studies'));
    }
}
