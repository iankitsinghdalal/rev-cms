<?php

namespace App\Http\Controllers;
use App\CompanyInformationModel;

use Illuminate\Http\Request;

class CompanyInfoController extends Controller
{
    public function getInfo()
    {
        //$CompanyDataView = CompanyInformationModel::all();
        return view('company_information.information')->with(['view' => $CompanyDataView]);
    }

    public function adminInforRequest(Request $request)
    {
        $CompanyData = new CompanyInformationModel([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'language' => $request->language,
        ]);
        $CompanyData->save();
        return back()->with('success', 'Company Information Saved!');
    }
}
