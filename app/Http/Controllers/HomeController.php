<?php

namespace App\Http\Controllers;

use App;
use App\CompanyInformationModel;
use App\SocialLinksModel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     * 
     * Guest Home
     */
    public function guestIndex()
    {
        return view('home.guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     * 
     * User Home
     */
    public function userIndex()
    {
        return view('home.user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     * 
     * Admin Home
     */
    public function adminIndex()
    {
        return view('home.admin');
    }

    /**
     * Set Locale for the application
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setLocale(Request $request)
    {
        session(['myLocale' => $request->input('locale')]);
        return redirect()->back();
    }

    /**
     * Show the Profile dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminProfile()
    {
        $compnayDta = CompanyInformationModel::all();
        $SocialData = SocialLinksModel::all();
        return view('company_information.information')->with(['view'=>$compnayDta, 'socialdata' => $SocialData]);
    }

    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }

    /** function for uploading image in page create action through function */
    public function createImage($imagename,$CompanyData,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $CompanyData->$imagename = $name;
    }

    /**
     * Insert Compnay Information Data.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminInforRequest(Request $request)
    {
        $CompanyData = new CompanyInformationModel([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'language' => $request->language,
        ]);
        if ($request->hasFile('image')) {
            $this::createImage('image',$CompanyData,$request);
	    }else{
            $CompanyData->image = NULL;
        }
        $CompanyData->save();
        return back()->with('success', 'Company Information Saved!');
    }

    public function InformationUpdate(Request $request)
    {
        $InfoUpdate = CompanyInformationModel::find($request->compnayinfobtn);
        $InfoUpdate->name = $request->name;
        $InfoUpdate->phone = $request->phone;
        $InfoUpdate->address = $request->address;
        $InfoUpdate->save();
        return back();
    }

    public function adminInfodelete(Request $request)
    {
        $infoDelete = CompanyInformationModel::find($request->delinfoid);
        $infoDelete->delete();
        return back()->with('delete', 'Social Data Delete!');
    }

    public function adminSocialLink(Request $request)
    {
        $SocialData = new SocialLinksModel([
            'social' => $request->social,
            'url' => $request->url
        ]);
        $SocialData->save();
        return back()->with(['socialdata' => $SocialData]);
    }


    public function adminsocialdelete(Request $request)
    {
        $SocialDelete = SocialLinksModel::find($request->delid);
        $SocialDelete->delete();
        return back()->with('delete', 'Social Data Delete!');
    }

    public function SocialUpdate(Request $request)
    {
        $SocialUpdate = SocialLinksModel::find($request->socialedit);
        $SocialUpdate->social = $request->social;
        $SocialUpdate->url = $request->url;
        $SocialUpdate->save();
        return back();
    }
    
}
