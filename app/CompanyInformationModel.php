<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInformationModel extends Model
{
    protected $table="company_information";
    protected $fillable=['name','phone','address','image','language'];
}
