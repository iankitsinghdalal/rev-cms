<?php

use Carbon\Carbon;
use App\Models\Gallery;
use Illuminate\Database\Seeder;

class GalleryTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['title' => 'Test Title A', 'path' => 'https://rev-cms.s3.ap-south-1.amazonaws.com/gallery/JXVk4G1rpFg04bfiWF8BSJ1Lyt9hj4T0T6BbUcXr.jpeg', 'size' => 77000.8, 'auth_by' => 1, 'created_at' => '2020-03-17 11:15:28', 'updated_at' => '2020-03-17 11:15:28'],
        ['title' => 'Test Title B', 'path' => 'https://rev-cms.s3.ap-south-1.amazonaws.com/gallery/JXVk4G1rpFg04bfiWF8BSJ1Lyt9hj4T0T6BbUcXr.jpeg', 'size' => 77000.8, 'auth_by' => 1, 'created_at' => '2020-03-17 11:15:28', 'updated_at' => '2020-03-17 11:15:28'],
        ['title' => 'Test Title C', 'path' => 'https://rev-cms.s3.ap-south-1.amazonaws.com/gallery/JXVk4G1rpFg04bfiWF8BSJ1Lyt9hj4T0T6BbUcXr.jpeg', 'size' => 77000.8, 'auth_by' => 1, 'created_at' => '2020-03-17 11:15:28', 'updated_at' => '2020-03-17 11:15:28'],
        ['title' => 'Test Title D', 'path' => 'https://rev-cms.s3.ap-south-1.amazonaws.com/gallery/JXVk4G1rpFg04bfiWF8BSJ1Lyt9hj4T0T6BbUcXr.jpeg', 'size' => 77000.8, 'auth_by' => 1, 'created_at' => '2020-03-17 11:15:28', 'updated_at' => '2020-03-17 11:15:28'],
        ['title' => 'Test Title E', 'path' => 'https://rev-cms.s3.ap-south-1.amazonaws.com/gallery/JXVk4G1rpFg04bfiWF8BSJ1Lyt9hj4T0T6BbUcXr.jpeg', 'size' => 77000.8, 'auth_by' => 1, 'created_at' => '2020-03-17 11:15:28', 'updated_at' => '2020-03-17 11:15:28'],
        ['title' => 'Test Title F', 'path' => 'https://rev-cms.s3.ap-south-1.amazonaws.com/gallery/JXVk4G1rpFg04bfiWF8BSJ1Lyt9hj4T0T6BbUcXr.jpeg', 'size' => 77000.8, 'auth_by' => 1, 'created_at' => '2020-03-17 11:15:28', 'updated_at' => '2020-03-17 11:15:28'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gallery::insert(self::DATA);
    }
}
