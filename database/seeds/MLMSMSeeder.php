<?php

use MenusTableSeeder as MTS;
use SubMenusTableSeeder as SMTS;
use MenuLocationTableSeeder as MLTS;
use App\Models\MenuLocation as ML;
use Illuminate\Database\Seeder;

class MLMSMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(MLTS::DATA as $key => $value) {
            $ml = ML::create($value);
            foreach(MTS::DATA[$value['name']] as $key => $value) {
                $m = $ml->menus()->create($value);
                foreach(SMTS::DATA[$value['name']] as $key => $value) {
                    $m->subMenus()->create($value);
                }
            }
        }
    }
}
