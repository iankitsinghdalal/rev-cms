<?php

use App\Models\Industry;
use Illuminate\Database\Seeder;

class IndustriesTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['title' => 'Test Industry A', 'fa_icon' => 'fas fa-dolly-flatbed ind-ico', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Industry::insert(self::DATA);
    }
}
