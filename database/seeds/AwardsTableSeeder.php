<?php

use App\Models\Award;
use Illuminate\Database\Seeder;

class AwardsTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['image' => 1, 'title' => 'Test Award A', 'description' => 'Test description', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Award::insert(self::DATA);
    }
}
