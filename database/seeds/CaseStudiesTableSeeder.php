<?php

use App\Models\CaseStudy;
use Illuminate\Database\Seeder;

class CaseStudiesTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['image' => 1, 'banner_image' => 1, 'slug' => 'test-slug-a', 'title' => 'Test Title A', 'meta_title' => 'Test Meta Title A', 'meta_description' => 'Test Meta Description A', 'keywords' => 'Test Keyword A1, Test Keyword A2,', 'description' => 'Test Description A', 'category_id' => null, 'author_name' => 'Test Author A', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CaseStudy::insert(self::DATA);
    }
}
