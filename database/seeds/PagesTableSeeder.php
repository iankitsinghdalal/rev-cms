<?php

use App\Models\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['slug' => 'test-slug-a', 'template_id' => null],
        ['slug' => 'test-slug-b', 'template_id' => null]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::insert(self::DATA);
    }
}
