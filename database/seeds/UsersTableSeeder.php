<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name'=>'Super Admin',
                'email'=>'superadmin@revinfotech.com',
                'role' => 'superadmin',
                'password'=> '123123',
            ],
            [
                'name'=>'Admin',
                'email'=>'admin@revinfotech.com',
                'role' => 'admin',
                'password'=> '123123',
            ],
            [
                'name'=>'User',
                'email'=>'user@revinfotech.com',
                'role' => 'user',
                'password'=> '123123',
            ],
            [
                'name'=>'Guest',
                'email'=>'guest@revinfotech.com',
                'role' => 'guest',
                'password'=> '123123',
            ],
        ];
  
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
