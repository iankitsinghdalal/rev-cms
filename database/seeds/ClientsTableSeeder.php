<?php

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['image' => 1, 'title' => 'Test Client A', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::insert(self::DATA);
    }
}
