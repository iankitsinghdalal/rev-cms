<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            GalleryTableSeeder::class,
            LanguagesTableSeeder::class,
            MLMSMSeeder::class,
            ClientsTableSeeder::class,
            PartnersTableSeeder::class,
            PortfoliosTableSeeder::class,
            TeamTableSeeder::class,
            AwardsTableSeeder::class,
            CaseStudiesTableSeeder::class,
            BlogsTableSeeder::class,
            JobOpeningsTableSeeder::class,
            PagesTableSeeder::class,
            IndustriesTableSeeder::class
        ]);
    }
}
