<?php

use Illuminate\Database\Seeder;

class SubMenusTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        'Home' => [
            ['name' => 'Item 1', 'status' => 0]
        ],
        'Blog' => [
            ['name' => 'Item 1', 'status' => 0]
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
