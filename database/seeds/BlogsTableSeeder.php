<?php

use App\Models\Blog;
use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{
    const DATA = [
        ['image' => 1, 'banner_image' => 1, 'slug' => 'Test Slug A', 'title' => 'Test Title A', 'meta_title' => 'Test Meta Title A', 'meta_description' => 'Test Meta Description A', 'keywords' => 'Test Keyword A1, Test Keyword A2', 'description' => 'Test Description A', 'category_id' => null, 'author_name' => 'Test Author Name A', 'author_image' => 1, 'author_content' => 'Test Author Content A', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Blog::insert(self::DATA);
    }
}
