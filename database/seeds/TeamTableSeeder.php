<?php

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['image' => 1, 'name' => 'Test Name A', 'description' => 'Test description', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::insert(self::DATA);
    }
}
