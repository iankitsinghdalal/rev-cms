<?php

use App\Models\Partner;
use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['image' => 1, 'title' => 'Test Partner A', 'description' => 'Test description', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Partner::insert(self::DATA);
    }
}
