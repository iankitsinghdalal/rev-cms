<?php

use App\Models\Portfolio;
use Illuminate\Database\Seeder;

class PortfoliosTableSeeder extends Seeder
{
    /**
     * 
     */
    const ARR = [
        "1"
    ];

    const DATA = [
        ['logo' => 1, 'feature_image' => 1, 'normal_images' => self::ARR, 'description' => 'Test Description A', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::DATA as $key => $value) {
            Portfolio::create($value);
        }
    }
}
