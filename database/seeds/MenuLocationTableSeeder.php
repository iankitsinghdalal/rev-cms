<?php

use App\Models\Menu;
use App\Models\MenuLocation;
use Illuminate\Database\Seeder;

class MenuLocationTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['name' => 'header', 'status' => 0],
        ['name' => 'footer', 'status' => 0],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
