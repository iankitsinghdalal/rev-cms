<?php

use App\Models\JobOpening;
use Illuminate\Database\Seeder;

class JobOpeningsTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['position' => 'Test Position A', 'department' => 'Test Development A', 'experience_required' => '1 - 5', 'location' => 'Test Location A', 'sharp_skills' => 'Test Sharp Skill A', 'skills' => 'Test Skill A', 'job_description' => 'Test Job Descripton A', 'remarks' => 'Test Remarks A', 'language_id' => 1, 'status' => 0]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobOpening::insert(self::DATA);
    }
}
