<?php

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        'header' => [
            ['name' => 'Home', 'language_id' => 1, 'status' => 0]
        ],
        'footer' => [
            ['name' => 'Blog', 'language_id' => 1, 'status' => 0]
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
