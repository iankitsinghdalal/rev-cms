<?php

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * 
     */
    const DATA = [
        ['name' => 'English', 'short_name' => 'en', 'status' => 1],
        ['name' => 'French', 'short_name' => 'fr', 'status' => 1],
        ['name' => 'Italian', 'short_name' => 'it', 'status' => 1],
        ['name' => 'Russian', 'short_name' => 'ru', 'status' => 1],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::insert(self::DATA);
    }
}
