<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\MenuLocation;
use Faker\Generator as Faker;

$factory->define(MenuLocation::class, function (Faker $faker) {
    return [
        'name' => strtolower('header')
    ];
});
