<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_openings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('position');
            $table->string('department');
            $table->string('experience_required');
            $table->string('location');
            $table->string('sharp_skills');
            $table->string('skills');
            $table->text('job_description');
            $table->text('remarks')->nullable();
            $table->unsignedBigInteger('language_id')->nullable();
            $table->boolean('status')->default(0);
            $table->bigInteger('order')->default(1);
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_openings');
    }
}
