<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('image')->nullable();
            $table->unsignedBigInteger('banner_image')->nullable();
            $table->string('slug');
            $table->string('title');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->text('keywords');
            $table->longText('description');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('author_name');
            $table->unsignedBigInteger('author_image')->nullable();
            $table->text('author_content');
            $table->unsignedBigInteger('language_id')->nullable();
            $table->boolean('status')->default(0);
            $table->bigInteger('order')->default(1);
            $table->timestamps();

            $table->foreign('image')->references('id')->on('gallery')->onDelete('set null');
            $table->foreign('banner_image')->references('id')->on('gallery')->onDelete('set null');
            $table->foreign('author_image')->references('id')->on('gallery')->onDelete('set null');
            #$table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
