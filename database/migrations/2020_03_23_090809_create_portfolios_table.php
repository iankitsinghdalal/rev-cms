<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('logo')->nullable();
            $table->unsignedBigInteger('feature_image')->nullable();
            $table->text('normal_images')->nullable();
            $table->text('description');
            $table->unsignedBigInteger('page_id')->nullable();
            $table->unsignedBigInteger('language_id')->nullable();
            $table->boolean('status')->default(0);
            $table->bigInteger('order')->default(1);
            $table->timestamps();

            $table->foreign('logo')->references('id')->on('gallery')->onDelete('set null');
            $table->foreign('feature_image')->references('id')->on('gallery')->onDelete('set null');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
